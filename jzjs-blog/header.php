<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package jzjs
 * @since jzjs 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'jzjs' ), max( $paged, $page ) );

	?></title>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'before' ); ?>
<header id="template-header" class="gradient-darkgray">
<div id="template-header-container">
	<section id="template-header-left" class="alignleft clearfix">
	
		<?php get_template_part('header-home-link'); ?>

	</section>
	<section id="template-header-right" class="alignright clearfix">
		<nav id="top-nav">
			<h3 class="visuallyhidden"><?php _e( 'Menu', 'jzjs' ); ?></h3>
			<div class="visuallyhidden skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'jzjs' ); ?>"><?php _e( 'Skip to content', 'jzjs' ); ?></a></div>

			<?php wp_nav_menu( array( 'theme_location' => 'top' ) ); ?>

		</nav>
		<section id="header-search">

			<?php get_template_part('searchform'); ?>

		</section>
		<section id="header-social-widget-area">
			
			<?php dynamic_sidebar('header-below-search'); ?>
			
		</section>
	</section>	
	<section id="header-ad"> </section>
	<nav id="main-nav" class="gradient-gray popout-gray">
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	</nav>
</div>
</header>

<section id="template-main">