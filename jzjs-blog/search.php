<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package jzjs
 * @since jzjs 1.0
 */

get_header(); ?>

	<section id="content">
		<aside id="above-post-widgets">
			
			<?php dynamic_sidebar('above-post'); ?>

		</aside>

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'jzjs' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</header>

				<?php jzjs_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'search' ); ?>

				<?php endwhile; ?>

				<?php jzjs_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'search' ); ?>

			<?php endif; ?>

		<aside id="below-post-widgets">
			
			<?php dynamic_sidebar('below-post'); ?>

		</aside>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>