<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package jzjs
 * @since jzjs 1.0
 */

get_header(); ?>

	<section id="content">
		<aside id="above-post-widgets">
			
			<?php dynamic_sidebar('above-post'); ?>

		</aside>

		<?php get_template_part( 'no-results', 'index' ); ?>

		<aside id="below-post-widgets">
			
			<?php dynamic_sidebar('below-post'); ?>

		</aside>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>