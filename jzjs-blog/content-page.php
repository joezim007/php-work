<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package jzjs
 * @since jzjs 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h2>
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php the_content(); ?>
		<br class="clear">
		<?php wp_link_pages( array( 
				'before' => '<div class="page-links">' . __( 'Pages:', 'jzjs' ), 
				'after' => '</div>' ,
				'link_before' => '<span class="box-link">',
				'link_after' => '</span>'
			) ); 
		?>
		<?php edit_post_link( __( 'Edit', 'jzjs' ), '<span class="edit-link">', '</span>' ); ?>
		
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
