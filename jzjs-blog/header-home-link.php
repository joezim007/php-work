<?php 
$jzjs_options = jzjs_get_theme_options();

if ($jzjs_options['heading_option'] == 'txt') : ?>
		<h1 id="site-title"><a href="<?php echo home_url( '/' ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<h2 id="site-tagline"><a href="<?php echo home_url( '/' ); ?>" rel="home"><?php bloginfo( 'description' ); ?></a></h2>
<?php else: ?>
	 	<a href="<?php echo home_url( '/' ); ?>" id="logo" rel="home"><img src="<?php echo $jzjs_options['logo_url'] ?>" alt="<?php bloginfo( 'name' ); ?>: <?php bloginfo( 'description' ); ?>" /></a>
<?php endif; ?>