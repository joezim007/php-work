<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package jzjs
 * @since jzjs 1.0
 */
?>

</section><!-- #template-main -->

<footer id="template-footer" class="gradient-darkgray popout-gray">
<div id="template-footer-container">
	<section id="footer-left">

		<?php dynamic_sidebar('footer-left'); ?>

	</section>
	<section id="footer-right">	

		<?php dynamic_sidebar('footer-right'); ?>

	</section>
	</section>
	<section id="footer-bottom">
		<nav id="footer-nav">
			
			<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>

		</nav>
		<div id="copyright">

			<?php dynamic_sidebar('footer-copyright'); ?>

		</div>	
	</section> <!-- #footer-bottom -->
</div> <!-- #template-footer-container -->
</footer> <!-- #template-footer -->

<?php wp_footer(); ?>

</body>
</html>