<?php
/**
 * The template for displaying search forms in jzjs
 *
 * @package jzjs
 * @since jzjs 1.0
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<label for="s" class="visuallyhidden"><?php _e( 'Search', 'jzjs' ); ?></label>
		<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search the Site', 'jzjs' ); ?>" />
		<input type="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'jzjs' ); ?>" />
	</form>
