<?php
/**
 * jzjs Theme Options
 *
 * @package jzjs
 * @since jzjs 1.0
 */

/**
 * Register the form setting for our jzjs_options array.
 *
 * This function is attached to the admin_init action hook.
 *
 * This call to register_setting() registers a validation callback, jzjs_theme_options_validate(),
 * which is used when the option is saved, to ensure that our option values are properly
 * formatted, and safe.
 *
 * @since jzjs 1.0
 */
function jzjs_theme_options_init() {
	register_setting(
		'jzjs_options',       // Options group, see settings_fields() call in jzjs_theme_options_render_page()
		'jzjs_theme_options', // Database option, see jzjs_get_theme_options()
		'jzjs_theme_options_validate' // The sanitization callback, see jzjs_theme_options_validate()
	);

	// Register our settings field group
	add_settings_section(
		'header', // Unique identifier for the settings section
		'Header', // Section title (we don't want one)
		'__return_false', // Section callback (we don't want anything)
		'theme_options' // Menu slug, used to uniquely identify the page; see jzjs_theme_options_add_page()
	);

	/*/ Register our individual settings fields
	add_settings_field(
		'sample_checkbox', // Unique identifier for the field for this section
		__( 'Sample Checkbox', 'jzjs' ), // Setting field label
		'jzjs_settings_field_sample_checkbox', // Function that renders the settings field
		'theme_options', // Menu slug, used to uniquely identify the page; see jzjs_theme_options_add_page()
		'general' // Settings section. Same as the first argument in the add_settings_section() above
	);/**/

	add_settings_field( 
		'logo',
		__( 'Logo Image URL', 'jzjs' ),
		'jzjs_logo_url',
		'theme_options',
		'header'
	);

	add_settings_field( 
		'heading-option',
		__( 'Heading Option', 'jzjs' ),
		'jzjs_heading_option',
		'theme_options',
		'header'
	);
	//add_settings_field( 'sample_select_options', __( 'Sample Select Options', 'jzjs' ), 'jzjs_settings_field_sample_select_options', 'theme_options', 'general' );
	//add_settings_field( 'sample_radio_buttons', __( 'Sample Radio Buttons', 'jzjs' ), 'jzjs_settings_field_sample_radio_buttons', 'theme_options', 'general' );
	//add_settings_field( 'sample_textarea', __( 'Sample Textarea', 'jzjs' ), 'jzjs_settings_field_sample_textarea', 'theme_options', 'general' );
}
add_action( 'admin_init', 'jzjs_theme_options_init' );

/**
 * Change the capability required to save the 'jzjs_options' options group.
 *
 * @see jzjs_theme_options_init() First parameter to register_setting() is the name of the options group.
 * @see jzjs_theme_options_add_page() The edit_theme_options capability is used for viewing the page.
 *
 * @param string $capability The capability used for the page, which is manage_options by default.
 * @return string The capability to actually use.
 */
function jzjs_option_page_capability( $capability ) {
	return 'edit_theme_options';
}
add_filter( 'option_page_capability_jzjs_options', 'jzjs_option_page_capability' );

/**
 * Add our theme options page to the admin menu.
 *
 * This function is attached to the admin_menu action hook.
 *
 * @since jzjs 1.0
 */
function jzjs_theme_options_add_page() {
	$theme_page = add_theme_page(
		__( 'Theme Options', 'jzjs' ),   // Name of page
		__( 'Theme Options', 'jzjs' ),   // Label in menu
		'edit_theme_options',          // Capability required
		'theme_options',               // Menu slug, used to uniquely identify the page
		'jzjs_theme_options_render_page' // Function that renders the options page
	);
}
add_action( 'admin_menu', 'jzjs_theme_options_add_page' );

/**
 * Returns the options array for jzjs.
 *
 * @since jzjs 1.0
 */
function jzjs_get_theme_options() {
	$saved = (array) get_option( 'jzjs_theme_options' );
	$defaults = array(
		'logo_url'       		=> '',
		'heading_option'     	=> 'txt',
	);

	$defaults = apply_filters( 'jzjs_default_theme_options', $defaults );

	$options = wp_parse_args( $saved, $defaults );
	$options = array_intersect_key( $options, $defaults );

	return $options;
}

/**
 * Renders the sample text input setting field.
 */
function jzjs_logo_url() {
	$options = jzjs_get_theme_options();
	?>
	<input type="text" name="jzjs_theme_options[logo_url]" id="sample-text-input" value="<?php echo esc_attr( $options['logo_url'] ); ?>" />
	<label class="description" for="sample-text-input"><?php _e( 'Relative or absolute URL to logo image.', 'jzjs' ); ?></label>
	<?php
}

/**
 * Returns an array of sample radio options registered for jzjs.
 *
 * @since jzjs 1.0
 */
function jzjs_heading_option_values() {
	$radio_buttons = array(
		'img' => array(
			'value' => 'img',
			'label' => __( 'Logo Image', 'jzjs' )
		),
		'txt' => array(
			'value' => 'txt',
			'label' => __( 'Site Name Text', 'jzjs' )
		)
	);

	return apply_filters( 'jzjs_heading_option', $radio_buttons );
}

/**
 * Renders the radio options setting field.
 *
 * @since jzjs 1.0
 */
function jzjs_heading_option() {
	$options = jzjs_get_theme_options();

	foreach ( jzjs_heading_option_values() as $button ) {
	?>
	<div class="layout">
		<label class="description">
			<input type="radio" name="jzjs_theme_options[heading_option]" value="<?php echo esc_attr( $button['value'] ); ?>" <?php checked( $options['heading_option'], $button['value'] ); ?> />
			<?php echo $button['label']; ?>
		</label>
	</div>
	<?php
	}
}

/**
 * Renders the sample checkbox setting field.
 *//*/
function jzjs_settings_field_sample_checkbox() {
	$options = jzjs_get_theme_options();
	?>
	<label for"sample-checkbox">
		<input type="checkbox" name="jzjs_theme_options[sample_checkbox]" id="sample-checkbox" <?php checked( 'on', $options['sample_checkbox'] ); ?> />
		<?php _e( 'A sample checkbox.', 'jzjs' );  ?>
	</label>
	<?php
}/**/

/**
 * Renders the sample text input setting field.
 *//*/
function jzjs_settings_field_sample_text_input() {
	$options = jzjs_get_theme_options();
	?>
	<input type="text" name="jzjs_theme_options[sample_text_input]" id="sample-text-input" value="<?php echo esc_attr( $options['sample_text_input'] ); ?>" />
	<label class="description" for="sample-text-input"><?php _e( 'Sample text input', 'jzjs' ); ?></label>
	<?php
}/**/

/**
 * Renders the sample select options setting field.
 *//*/
function jzjs_settings_field_sample_select_options() {
	$options = jzjs_get_theme_options();
	?>
	<select name="jzjs_theme_options[sample_select_options]" id="sample-select-options">
		<?php
			$selected = $options['sample_select_options'];
			$p = '';
			$r = '';

			foreach ( jzjs_sample_select_options() as $option ) {
				$label = $option['label'];
				if ( $selected == $option['value'] ) // Make default first in list
					$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
				else
					$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
			}
			echo $p . $r;
		?>
	</select>
	<label class="description" for="sample_theme_options[selectinput]"><?php _e( 'Sample select input', 'jzjs' ); ?></label>
	<?php
}/**/

/**
 * Renders the sample textarea setting field.
 *//*/
function jzjs_settings_field_sample_textarea() {
	$options = jzjs_get_theme_options();
	?>
	<textarea class="large-text" type="text" name="jzjs_theme_options[sample_textarea]" id="sample-textarea" cols="50" rows="10" /><?php echo esc_textarea( $options['sample_textarea'] ); ?></textarea>
	<label class="description" for="sample-textarea"><?php _e( 'Sample textarea', 'jzjs' ); ?></label>
	<?php
}/**/

/**
 * Returns an array of sample select options registered for jzjs.
 *
 * @since jzjs 1.0
 *//*/
function jzjs_sample_select_options() {
	$sample_select_options = array(
		'0' => array(
			'value' =>	'0',
			'label' => __( 'Zero', 'jzjs' )
		),
		'1' => array(
			'value' =>	'1',
			'label' => __( 'One', 'jzjs' )
		),
		'2' => array(
			'value' => '2',
			'label' => __( 'Two', 'jzjs' )
		),
		'3' => array(
			'value' => '3',
			'label' => __( 'Three', 'jzjs' )
		),
		'4' => array(
			'value' => '4',
			'label' => __( 'Four', 'jzjs' )
		),
		'5' => array(
			'value' => '3',
			'label' => __( 'Five', 'jzjs' )
		)
	);

	return apply_filters( 'jzjs_sample_select_options', $sample_select_options );
}/**/

/**
 * Renders the Theme Options administration screen.
 *
 * @since jzjs 1.0
 */
function jzjs_theme_options_render_page() {
	?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<h2><?php printf( __( '%s Theme Options', 'jzjs' ), get_current_theme() ); ?></h2>
		<?php settings_errors(); ?>

		<form method="post" action="options.php">
			<?php
				settings_fields( 'jzjs_options' );
				do_settings_sections( 'theme_options' );
				submit_button();
			?>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate form input. Accepts an array, return a sanitized array.
 *
 * @see jzjs_theme_options_init()
 * @todo set up Reset Options action
 *
 * @param array $input Unknown values.
 * @return array Sanitized theme options ready to be stored in the database.
 *
 * @since jzjs 1.0
 */
function jzjs_theme_options_validate( $input ) {
	$output = array();

	/*/ Checkboxes will only be present if checked.
	if ( isset( $input['logo_url'] ) )
		$output['sample_checkbox'] = 'on';/**/

	// The sample text input must be safe text with no HTML tags
	if ( isset( $input['logo_url'] ) && ! empty( $input['logo_url'] ) )
		$output['logo_url'] = wp_filter_nohtml_kses( $input['logo_url'] );

	/*/ The sample select option must actually be in the array of select options
	if ( isset( $input['sample_select_options'] ) && array_key_exists( $input['sample_select_options'], jzjs_sample_select_options() ) )
		$output['sample_select_options'] = $input['sample_select_options'];
/**/
	// The sample radio button value must be in our array of radio button values
	if ( isset( $input['heading_option'] ) && array_key_exists( $input['heading_option'], jzjs_heading_option_values() ) )
		$output['heading_option'] = $input['heading_option'];
/***
	// The sample textarea must be safe text with the allowed tags for posts
	if ( isset( $input['sample_textarea'] ) && ! empty( $input['sample_textarea'] ) )
		$output['sample_textarea'] = wp_filter_post_kses( $input['sample_textarea'] );
/**/
	return apply_filters( 'jzjs_theme_options_validate', $output, $input );
}
