<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package jzjs
 * @since jzjs 1.0
 */

if ( ! function_exists( 'jzjs_content_nav' ) ):
/**
 * Display navigation to next/previous pages when applicable
 *
 * @since jzjs 1.0
 */
function jzjs_content_nav( ) {
	global $wp_query;

	$nav_class = 'paging-navigation';
	if ( is_single() ) :
		$nav_class = 'post-navigation';
	elseif ( is_attachment() ) :
		$nav_class = 'attachment-navigation';
	endif;

	?>
	<nav role="navigation" class="entry-pagination site-navigation <?php echo $nav_class; ?>">
		<h4 class="visuallyhidden">Post navigation</h4>

		<?php 
		// navigation links for attachments
		if ( is_attachment() ) :

			previous_image_link( false, '<span class="prev-post box-link">Previous Attachment</span>' );
			next_image_link( false, '<span class="next-post box-link">Next Attachment</span>' );

		// navigation links for single posts
		elseif ( is_single() ) :

			previous_post_link( '%link', '<span class="prev-post box-link">%title</span>' );
			next_post_link( '%link', '<span class="next-post box-link">%title</span>' );

		// navigation links for home, archive, and search pages
		elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) :

			if ( get_next_posts_link() ) :
				next_posts_link( __( '<span class="old-posts box-link">Older posts</span>', 'jzjs' ) );
			endif;

			if ( get_previous_posts_link() ) :
				previous_posts_link( __( '<span class="new-posts box-link">Newer posts</span>', 'jzjs' ) );
			endif;

		endif; ?>

	</nav><!-- .entry-pagination -->
	<?php
}
endif; // jzjs_content_nav

if ( ! function_exists( 'jzjs_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since jzjs 1.0
 */
function jzjs_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'jzjs' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'jzjs' ), ' ' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'jzjs' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'jzjs' ); ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time pubdate datetime="<?php comment_time( 'c' ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( __( '%1$s at %2$s', 'jzjs' ), get_comment_date(), get_comment_time() ); ?>
					</time></a>
					<?php edit_comment_link( __( '(Edit)', 'jzjs' ), ' ' );
					?>
				</div><!-- .comment-meta .commentmetadata -->
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for jzjs_comment()

if ( ! function_exists( 'jzjs_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since jzjs 1.0
 */
function jzjs_posted_on( $post = null ) {
	$image = wp_get_attachment_metadata(); ?>

	<li class="entry-date"><?php the_date() ?></li>
	<?php if ( has_category() ) : ?><li class="entry-category"><img src="<?php echo get_template_directory_uri() ?>/images/category.png" alt="categories" height=16 width=16> <?php the_category( __( ', ', 'jzjs' ) ) ?></li><?php endif; ?>
	<?php if ( has_tag() ) : ?><li class="entry-tags"><img src="<?php echo get_template_directory_uri() ?>/images/tag.png" alt="tags" height=16 width=16> <?php the_tags( '', __( ', ', 'jzjs' ) ) ?></li><?php endif; ?>
	<?php if ( !is_attachment() ) : ?><li class="entry-comments"><img src="<?php echo get_template_directory_uri() ?>/images/comment.png" alt="comments" height=16 width=16> <?php comments_popup_link() ?></li><?php endif; ?>
	<?php if ( is_attachment() ) : ?>
	<li class="entry-attach-size">Original Size: <?php echo $image['width'] . " x " . $image['height']; ?></li>
	<li class="entry-attach-src"><a href="<?php echo wp_get_attachment_url(); ?>">Original Source</a></li>
	<li class="entry-parent-post">Attached To: <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a></li>
	<?php endif; ?>
	<?php edit_post_link( __( 'Edit', 'jzjs' ), '<li class="entry-edit-link">', '</li>' ); ?>
	
<?php
}

endif;

/**
 * Returns true if a blog has more than 1 category
 *
 * @since jzjs 1.0
 */
function jzjs_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so jzjs_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so jzjs_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in jzjs_categorized_blog
 *
 * @since jzjs 1.0
 */
function jzjs_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'jzjs_category_transient_flusher' );
add_action( 'save_post', 'jzjs_category_transient_flusher' );