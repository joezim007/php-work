<?php
/**
 * jzjs functions and definitions
 *
 * @package jzjs
 * @since jzjs 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since jzjs 1.0
 */

if ( ! function_exists( 'jzjs_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since jzjs 1.0
 */
function jzjs_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	//require( get_template_directory() . '/inc/tweaks.php' );

	/**
	 * Custom Theme Options
	 */
	require( get_template_directory() . '/inc/theme-options/theme-options.php' );

	/**
	 * WordPress.com-specific functions and definitions
	 */
	//require( get_template_directory() . '/inc/wpcom.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on jzjs, use a find and replace
	 * to change 'jzjs' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'jzjs', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Register Menu Locations
	 */

	register_nav_menus( array(
		'top' => __( 'Top Header Menu', 'jzjs' ),
	) );

	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'jzjs' ),
	) );

	register_nav_menus( array(
		'footer' => __( 'Footer Menu', 'jzjs' ),
	) );

	/**
	 * Add support for the Aside Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', ) );
}
endif; // jzjs_setup
add_action( 'after_setup_theme', 'jzjs_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since jzjs 1.0
 */
function jzjs_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Header Below Search', 'jzjs' ),
		'id' => 'header-below-search',
		'before_widget' => '<aside id="%1$s" class="header-widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Above Post', 'jzjs' ),
		'id' => 'above-post',
		'before_widget' => '<aside id="%1$s" class="content-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Below Post', 'jzjs' ),
		'id' => 'below-post',
		'before_widget' => '<aside id="%1$s" class="content-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Sidebar', 'jzjs' ),
		'id' => 'sidebar',
		'before_widget' => '<aside id="%1$s" class="sidebar-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Left', 'jzjs' ),
		'id' => 'footer-left',
		'before_widget' => '<aside id="%1$s" class="footer-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Right', 'jzjs' ),
		'id' => 'footer-right',
		'before_widget' => '<aside id="%1$s" class="footer-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Copyright', 'jzjs' ),
		'id' => 'footer-copyright',
		'before_widget' => '<aside id="%1$s" class="footer-widget widget clearfix %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'jzjs_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function jzjs_scripts() {
	global $post;

	wp_enqueue_style( 'style', get_stylesheet_uri() );

	//wp_enqueue_script( 'small-menu', get_template_directory_uri() . '/js/small-menu.js', array( 'jquery' ), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*if ( is_singular() && wp_attachment_is_image( $post->ID ) ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}*/
}
add_action( 'wp_enqueue_scripts', 'jzjs_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );