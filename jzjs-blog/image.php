<?php
/**
 * The template for displaying image attachments.
 *
 * @package jzjs
 * @since jzjs 1.0
 */

get_header();
?>

	<section id="content">
		<aside id="above-post-widgets">
			
			<?php dynamic_sidebar('above-post'); ?>

		</aside>

		<?php while ( have_posts() ) : 
			the_post();
		?>

			<article id="post-<?php the_ID(); ?>" <?php post_class("entry"); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>

					<ul class="entry-meta">
						<?php jzjs_posted_on( $post ); ?>
					</ul><!-- .entry-meta -->

				</header><!-- .entry-header -->

				<?php jzjs_content_nav() ?>

				<div class="entry-content">
					<div class="entry-attachment">
						<a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment">
							<?php echo wp_get_attachment_image( $post->ID, 'full' ); ?>
						</a>
					</div><!-- .attachment -->

					<?php if ( ! empty( $post->post_excerpt ) ) : ?>
					<div class="entry-caption">
						<?php the_excerpt(); ?>
					</div>
					<?php endif; ?>

					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'jzjs' ), 'after' => '</div>' ) ); ?>

				</div>
			</article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; // end of the loop. ?>

		<aside id="below-post-widgets">
			
			<?php dynamic_sidebar('below-post'); ?>

		</aside>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>