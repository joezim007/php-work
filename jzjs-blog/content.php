<?php
/**
 * @package jzjs
 * @since jzjs 1.0
 */

$class = "entry";
if ( is_sticky() ) {
	$class .= " sticky";
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'jzjs' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
		<ul class="entry-meta">
			<?php jzjs_posted_on(); ?>
		</ul><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading &rarr;', 'jzjs' ) ); ?>
		<br class="clear">
	</div><!-- .entry-content -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
<hr class="entry-divider" />
