<?php
/**
 * The Template for displaying all single posts.
 *
 * @package jzjs
 * @since jzjs 1.0
 */

get_header(); ?>

	<section id="content">
		<aside id="above-post-widgets">
			
			<?php dynamic_sidebar('above-post'); ?>

		</aside>
		
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'single' ); ?>

					<?php jzjs_content_nav( 'nav-below' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template( '', true );
					?>

				<?php endwhile; // end of the loop. ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>

		<aside id="below-post-widgets">
			
			<?php dynamic_sidebar('below-post'); ?>

		</aside>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>