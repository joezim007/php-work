jQuery.noConflict();

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
};
String.prototype.isNumber = function() {
	return (this.match(/^\d*\.?\d+$/) && !isNaN(Number(this)));
};
String.prototype.isInteger = function() {
	return (this.match(/^\d+$/) && !isNaN(Number(this)));
};
String.prototype.isEmpty = function() {
	var val = this.trim();
	return (val.length === 0 || val === "");
};
	

var WCTS = (function() {

	var initDatePicker = function() {
		jQuery("#date").datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
		
		jQuery("#ui-datepicker-div").bind('click', function() {
			WCTS.form.validateField(jQuery("#date").get(0));
		});
	}
	
	var initTable = function() {		
		if (jQuery("input[name^='row']").length == 0) {
			WCTS.table.addRow();
		} 
		else {
			WCTS.form.setListeners();
			WCTS.table.updateTotal();
			WCTS.table.stripe();
		}
	}

	var initToolTips = function() {
		jQuery(':input').wTooltip({
			className: "errortooltip",
			condition: function(tooltip, node, settings) {
				$node = jQuery(node);
				
				if ($node.hasClass('errorint')) {			
					jQuery(tooltip).text("This value must be an integer.").show();
					return true;
				}
				else if ($node.hasClass('errornum')) {
					jQuery(tooltip).text("This value must be an number.").show();
					return true;
				}
				else if ($node.hasClass('error')) {
					jQuery(tooltip).text("Please enter or select a value.").show();
					return true;
				}
				
				return false;
			},
			content: 'dummy content'
		});
	}

	return {
		init : function() {
			initDatePicker();
			initTable();
			initToolTips();
		},
		table : {
			addRow : function() {
				$rows = jQuery("input[name^='row']");
				if ($rows.length > 0)
					num = Number($rows.last().attr('name').replace(/[a-zA-Z]+/i, '')) + 1;
				else {
					num = 0;
				}
				
				jQuery("<tr>"+
					"<td><input type=\"checkbox\" name=\"row"+num+"\" /></td>"+
					"<td><input type=\"text\" name=\"quantity["+num+"]\" class=\"int\" /></td>"+
					"<td>"+
						"<select name=\"volume["+num+"]\">"+
							"<option value=\"0\" selected=\"selected\">-Select-</option>"+
							"<option value=\"0.750\">750mL</option>"+
							"<option value=\"0.375\">375mL</option>"+
							"<option value=\"0.187\">187mL</option>"+
							"<option value=\"0.355\">12oz</option>"+
							"<option value=\"0.473\">16oz</option>"+
						"</select>"+
					"</td>"+
					"<td><input type=\"text\" name=\"description["+num+"]\" /></td>"+
					"<td>"+
						"<select name=\"bevclass["+num+"]\">"+
							"<option value=\"0\" selected=\"selected\">-Select-</option>"+
							"<option value=\"1\">Apple Cider 7% or less</option>"+
							"<option value=\"2\">Wine 14% or less</option>"+
							"<option value=\"3\">Wine over 14%</option>"+
						"</select>"+
					"</td>"+
					"<td><input type=\"text\" name=\"unitprice["+num+"]\" class=\"num\" /></td>"+
					"<td><input type=\"text\" name=\"itemtotal["+num+"]\" class=\"autofill\" disabled=\"disabled\" /></td>"+
				"</tr>"
				).appendTo(".order_table tbody").find(':input').wTooltip({
					className: "errortooltip",
					condition: function(tooltip, node, settings) {
						$node = jQuery(node);
						
						if ($node.hasClass('errorint')) {
						
							jQuery(tooltip).text("This value must be an integer.").show();
							return true;
						}
						else if ($node.hasClass('errornum')) {
							jQuery(tooltip).text("This value must be an number.").show();
							return true;
						}
						else if ($node.hasClass('error')) {
							jQuery(tooltip).text("Please enter or select a value.").show();
							return true;
						}
						
						return false;
					},
					content: 'dummy content'
				});;
				
				WCTS.table.stripe();
				WCTS.form.setListeners();
			},
			removeRow : function() {
				if (jQuery("input[name^='row']").filter("[checked='false']").length == 0) {
					WCTS.table.addRow();
				}
				jQuery("input#select_all").attr('checked', false);
				jQuery("input[name^='row']").filter("[checked='true']").closest('tr').remove();
				jQuery("tbody tr").each(function(i) {
					jQuery(this)
					.find("input[name^='row']").attr('name', 'row'+i).end()
					.find("input[name^='quantity']").attr('name', 'quantity['+i+']').end()
					.find("select[name^='volume']").attr('name', 'volume['+i+']').end()
					.find("input[name^='description']").attr('name', 'description['+i+']').end()
					.find("select[name^='bevclass']").attr('name', 'bevclass['+i+']').end()
					.find("input[name^='unitprice']").attr('name', 'unitprice['+i+']').end()
					.find("input[name^='itemtotal']").attr('name', 'itemtotal['+i+']');
				});
				
				WCTS.table.updateTotal();
				WCTS.table.stripe();
			},
			stripe : function() {			
				jQuery("tbody tr:odd").addClass("odd").removeClass('even');
				jQuery("tbody tr:even").addClass("even").removeClass('odd');
			},
			updateTotal : function() {
				var subtotal = 0;
				jQuery("input[name*='itemtotal']").each(function() {
					subtotal += Number(jQuery(this).val());
					if (isNaN(subtotal)) {
						subtotal = 0;
						return false;
					}
				});
				
				var $s = jQuery("input[name='subtotal']");
				if (Number($s.val()) != subtotal) {
					$s.val(subtotal.toFixed(2));
				}
			},
			checkAll : function() {
				var checked = jQuery(this).attr('checked');
				jQuery("input[name^='row']").attr('checked', checked);
			},
			updateChecked : function () {
				allSelected = true;
				
				jQuery("input[name^='row']").each(function() {
					if( !this.checked ) {
						allSelected = false;
						return false;
					}
				});
				
				jQuery('input#select_all').attr('checked', allSelected);
			}
		},
		invoice : {
			showBillToList : function() {
				jQuery("#billto_list").show();
				return false;
			},
			hideBillToList : function() {
				jQuery("#billto_list").hide();
			},
			updateInvoiceNum : function() {
				var wVal = jQuery("#winery").val();
				var oVal = jQuery("#ordernum").val();
				
				if (wVal != '0' && !oVal.isEmpty()) {
					jQuery("#invoicenum").val(wVal + oVal);
				}
			},
			updateBillTo : function() {					
				var $li = jQuery(this);
				
				WCTS.form.validateField(jQuery("#billto").val($li.children("span").text()).get(0));
				WCTS.form.validateField(jQuery("#address").val($li.find(".biz_address").text()).get(0));
				WCTS.form.validateField(jQuery("#csz").val($li.find(".biz_csz").text()).get(0));
				WCTS.form.validateField(jQuery("#resellernum").val($li.find(".biz_resell").text()).get(0));
				
				WCTS.invoice.hideBillToList();
			}
		},
		form : {
			setListeners : function() {		
				jQuery("input, select").not(".autofill").bind('blur change keyup', function() {
					WCTS.form.validateField(this);
				});				
	
				jQuery("input[name^='row']").bind('click change', function () {
					allSelected = true;
					
					jQuery("input[name^='row']").each(function() {
						if( !this.checked ) {
							allSelected = false;
							return false;
						}
					});
					
					jQuery('input#select_all').attr('checked', allSelected);
				});
				
				jQuery("input[name^='quantity'], input[name^='unitprice']")
				.bind('change keyup', function() {
					var number = jQuery(this).attr('name').replace(/[a-zA-Z]+/, '');
					var $q = jQuery("input[name='quantity"+number+"']");
					var $p = jQuery("input[name='unitprice"+number+"']");
					var qval = $q.val().trim();
					var pval = $p.val().trim();
					
					if(qval != "" && pval != "") {
						var $t = jQuery("input[name='itemtotal"+number+"']");
						var tval = qval * pval;
						if (!isNaN(tval)) {
							$t.val(tval.toFixed(2));
						} else {
							$t.val("0.00");
						}
					} else {
						jQuery("input[name='itemtotal"+number+"']").val('');
					}
					
					WCTS.table.updateTotal();
				});
			},
			validateField : function(field) {
				var $el = jQuery(field);
				$el.removeClass('errorint').removeClass('errornum').removeClass('error');	
				
				var val = $el.val().trim();
				
				if ($el.hasClass('int') && !val.isInteger()) {
					$el.addClass('errorint');
					return false;
				} else if ($el.hasClass('num') && !val.isNumber()) {
					$el.addClass('errornum');
					return false;
				} else if (!val || val == '0' || val.isEmpty()) {
					$el.addClass('error');
					return false;
				}
				
				return true;		
			},
			submitForm : function()  {
			
				var $fields  = jQuery("#invoice_form input, #invoice_form select").not(".autofill");
				var valid = true;
				
				$fields.each(function() {
					if (!WCTS.form.validateField(this)) {
						valid = false;
					}
				});
				
				if (!valid) {
					jQuery('.modal').modal({overlayClose:true, opacity:50, escClose:true});
					
					jQuery('a.close').click(function(e) {
						$.modal.close();
						e.preventDefault();
					});
				}
				
				return valid;
			}
		}
	};
})();

jQuery(function() {

	WCTS.init();
	
	jQuery("input, select").not("#billto").bind('focus', WCTS.invoice.hideBillToList);
	
	jQuery("#billto_list li").bind('click', WCTS.invoice.updateBillTo);
	
	jQuery("#winery, #ordernum").bind('change', WCTS.invoice.updateInvoiceNum);
	
	jQuery("#billto").bind('click focus', WCTS.invoice.showBillToList);
	
	jQuery("body").bind('click', WCTS.invoice.hideBillToList);
	
	jQuery("input#select_all").bind('click change', WCTS.table.checkAll);
	
	jQuery("#add_row").bind('click', WCTS.table.addRow);
	
	jQuery("#del_row").bind('click', WCTS.table.removeRow);
	
	jQuery("#invoice_form").bind('submit', WCTS.form.submitForm);
});