<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelInvoices extends JModel
{
	/**
	 * Partner id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_pages = 0;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		$this->setId( JRequest::getInt('id', 0) );
		parent::__construct();
	}

	/**
	 * Method to set the partner identifier
	 *
	 * @access	public
	 * @param	int Partner identifier
	 */
	function setId($id)
	{
		// Set partner id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
	
	function getPages($user) {
		if (!$this->_pages) {
		
			$where = $user->isAdmin || $user->isBanker ? '' : 'WHERE winery = '.$this->_db->Quote($user->abbr);
		
			$query = 'SELECT COUNT(id) '
					.'FROM #__wicitswinerycoop_invoices '
					.'WHERE id IN ( '
					.'	SELECT MAX(id) '
					.'	FROM #__wicitswinerycoop_invoices '
					.	$where
					.'	GROUP BY order_num, winery '
					.') ';
					
			$this->_db->setQuery($query);
			$this->_pages = ceil($this->_db->loadResult() / 20);
		}
		
		return (int)$this->_pages;
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData($user)
	{
		// Load the partner data
		if (!$this->_loadData($user))
		{
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Method to load content partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData($user)
	{
		$page  = JRequest::getInt('pg', 0);
		$limit = $page ? 'LIMIT '.(($page-1) * 20).', 20' : 'LIMIT 10';
		
		$where = $user->isAdmin || $user->isBanker ? '' : 'WHERE winery = '.$this->_db->Quote($user->abbr);
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$query = 'SELECT * '
					.'FROM #__wicitswinerycoop_invoices '
					.'WHERE id IN ( '
					.'	SELECT MAX(id) '
					.'	FROM #__wicitswinerycoop_invoices '
					.	$where
					.'	GROUP BY order_num, winery '
					.') '
					.'ORDER BY id DESC '
					.$limit;
					
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObjectList();
			
			if( !$this->_data ) {
				return false;
			}
			
			$query = 'SELECT invoice, SUM(qty) as qty '
					.'FROM #__wicitswinerycoop_invoice_items '
					.'WHERE invoice IN ( '
					.'	SELECT MAX(id) '
					.'	FROM #__wicitswinerycoop_invoices '
					.	$where
					.'	GROUP BY order_num, winery '
					.') '
					.'GROUP BY invoice '
					.'ORDER BY invoice DESC ';
			
			$this->_db->setQuery($query);
			$items = $this->_db->loadObjectList();
			
			foreach ($this->_data as &$invoice) {
				$invoice->numItems = 0;
				
				foreach($items as $item) {
					if ($item->invoice == $invoice->id) {
						$invoice->numItems = $item->qty;
						break;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Method to initialise the partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$this->_data = null;
			return (boolean) $this->_data;
		}
		return true;
	}
}
