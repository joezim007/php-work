<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelInvoice extends JModel
{
	/**
	 * Partner id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		$this->setId( JRequest::getInt('id', 0) );
		parent::__construct();
	}

	/**
	 * Method to set the partner identifier
	 *
	 * @access	public
	 * @param	int Partner identifier
	 */
	function setId($id)
	{
		// Set partner id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		// Load the partner data
		if (!$this->_loadData())
		{
			$this->_initData();
		}

		return $this->_data;
	}
	
	/**
	 * Method to change the paid status on an invoice (paid or unpaid)
	 *
	 * @access public
	 * @return boolean True on success, false otherwise.
	 */
	function payInvoice($data) {
		$id = $data[ 'id' ];
		
		$query = 'UPDATE #__wicitswinerycoop_invoices '.
				 'SET paid = 1 - paid '.
				 'WHERE id = '. $this->_db->Quote($id) .';';
		
		$this->_db->setQuery($query);
		
		if ( !$this->_db->query() ) {			
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		return true;
	}

	/**
	 * Method to store the partner
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function store($data)
	{
		jimport('joomla.utilities.date');
		
		$date         = $data[ 'date' ];
		$winery       = $data[ 'winery' ];
		$order_num    = $data[ 'ordernum' ];
		$phone        = $data[ 'phone' ];
		$bill_to      = $data[ 'billto' ];
		$address      = $data[ 'address' ];
		$address2     = $data[ 'csz' ];
		$reseller_num = $data[ 'resellernum' ];
		
		$session =& JFactory::getSession();
		$user = $session->get('winery_user', null);
		
		$date = new JDate($date);
		$date = $date->toMySQL();
		
		$query = 'INSERT INTO #__wicitswinerycoop_invoices '. 
				 '(date, winery, order_num, phone, bill_to, address, address2, reseller_num, created, user) '.
				 'VALUES ('.$this->_db->Quote($date).
				 ', '.$this->_db->Quote($winery).
				 ', '.$this->_db->Quote($order_num).
				 ', '.$this->_db->Quote($phone).
				 ', '.$this->_db->Quote($bill_to).
				 ', '.$this->_db->Quote($address).
				 ', '.$this->_db->Quote($address2).
				 ', '.$this->_db->Quote($reseller_num).
				 ', NOW()'.
				 ', '.$this->_db->Quote($user->id).');';
		
		$this->_db->setQuery($query);
		
		if ( !$this->_db->query() ) {			
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		$invoice_id  = $this->_db->insertid();
		$quantity    = $data[ 'quantity' ];
		$volume      = $data[ 'volume' ];
		$description = $data[ 'description' ];
		$bev_class   = $data[ 'bevclass' ];
		$unit_price  = $data[ 'unitprice' ];
		
		$length = count($quantity);
		
		if ( !$length ) {
			$this->setError('There were no items in the invoice.');
			return false;
		}
		
		$query = 'INSERT INTO #__wicitswinerycoop_invoice_items '.
				 '(qty, volume, description, beverage_class, unit_price, invoice) '.
				 'VALUES (';
				 
		for ($i = 0; $i < $length; $i++) {
			$query .= $this->_db->Quote($quantity[$i]).
				 ', '.$this->_db->Quote($volume[$i]).
				 ', '.$this->_db->Quote($description[$i]).
				 ', '.$this->_db->Quote($bev_class[$i]).
				 ', '.$this->_db->Quote($unit_price[$i]).
				 ', '.$this->_db->Quote($invoice_id).
				 '), (';
		}
		
		$query = substr($query, 0, -3);
		
		$this->_db->setQuery($query);
		
		if ( !$this->_db->query() ) {			
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		return true;
	}

	/**
	 * Method to load content partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$query = 'SELECT id, date, winery, order_num, phone, bill_to, address, address2, reseller_num, created, user '
					.'FROM #__wicitswinerycoop_invoices '
					.'WHERE id = '. $this->_db->Quote($this->_id)
					.' LIMIT 1';
					
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
			
			if( !$this->_data ) {
				return false;
			}
			
			$query = 'SELECT id, qty, volume, description, beverage_class, unit_price '
					.'FROM #__wicitswinerycoop_invoice_items '
					.'WHERE invoice = '. $this->_db->Quote($this->_id);
			
			$this->_db->setQuery($query);
			$this->_data->items = $this->_db->loadObjectList();
			
			return (boolean) $this->_data;
		}
		return true;
	}

	/**
	 * Method to initialise the partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$invoice = new stdClass();
			$invoice->id			= 0;
			$invoice->date			= 0;
			$invoice->winery		= 0;
			$invoice->order_num 	= 0;
			$invoice->phone 		= null;
			$invoice->bill_to 		= null;
			$invoice->address 		= null;
			$invoice->address2 		= null;
			$invoice->reseller_num	= null;
			$invoice->user 			= 0;
			$invoice->items 		= null;
			$this->_data			= $invoice;
			return (boolean) $this->_data;
		}
		return true;
	}
}
