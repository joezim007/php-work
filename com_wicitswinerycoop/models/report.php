<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelReport extends JModel
{
	/**
	 * Partner id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Temporary database data for the invoices
	 *
	 * @var array
	 */
	var $_invoices = null;
	
	/**
	 * Temporary database data for the invoice items
	 *
	 * @var array
	 */
	var $_items = null;
	
	/**
	 * Temporary database data for the Bank percentage
	 *
	 * @var array
	 */
	var $_perc = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		$this->setId( JRequest::getInt('id', 0) );
		parent::__construct();
	}

	/**
	 * Method to set the partner identifier
	 *
	 * @access	public
	 * @param	int Partner identifier
	 */
	function setId($id)
	{
		// Set partner id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData($user)
	{
		// Load the partner data
		if (!$this->_loadData($user))
		{
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Method to load content partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData($user)
	{
		if (empty($this->_data))
		{
		
			if($user->isAdmin) //don't forget to add the date lookup stuff
			{
				$query = 
					'SELECT w.name as winery_name, i.order_num, i.date, i.paid, i.bill_to, i.id, w.bank_percentage '
					. 'FROM #__wicitswinerycoop_invoices i, #__wicitswinerycoop_wineries w '
					. 'WHERE w.abbr = i.winery AND '
					. 'i.id IN ( '
						.'	SELECT MAX(id) '
						.'	FROM #__wicitswinerycoop_invoices'
						.'  GROUP BY order_num, winery'
					. ');';
					
				$this->_db->setQuery($query);
				$this->_invoices = $this->_db->loadObjectList();
			}
			else
			{
				$query =  
					'SELECT i.order_num, i.date, i.created, i.paid, i.bill_to, i.id, w.bank_percentage '
					. 'FROM #__wicitswinerycoop_invoices as i '
					. 'JOIN #__wicitswinerycoop_wineries as w ON w.abbr = ' . $this->_db->Quote($user->abbr)
					. 'WHERE i.winery = ' . $this->_db->Quote($user->abbr) . ' AND '
					. 'i.id IN ( '
						.'	SELECT MAX(id) '
						.'	FROM #__wicitswinerycoop_invoices'
						.'  WHERE winery = ' . $this->_db->Quote($user->abbr)
						.'  GROUP BY order_num, winery'
					. ');';
					
				$this->_db->setQuery($query);
				$this->_invoices = $this->_db->loadObjectList();
			}
			
			
			//get invoice items to be sorted through in view.html file
			$query =
				'SELECT qty, volume, unit_price, invoice '
				.'FROM #__wicitswinerycoop_invoice_items '
				.'WHERE invoice IN( '
				.'SELECT MAX(id) '
				.'FROM #__wicitswinerycoop_invoices ';
			if(!$user->isAdmin) {
				$query .= 'WHERE winery = ' . $this->_db->Quote($user->abbr);
			}
			$query .= 'GROUP BY order_num, winery );'; 
				
			$this->_db->setQuery($query);
			$this->_items = $this->_db->loadObjectList();
			
			$this->_insert_values();
			
			if( !$this->_data ) {
				return false;
			}
		}
		
		return true;
		
	}

	/**
	 * Method to initialise the partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$this->_data = null;
			return (boolean) $this->_data;
		}
		return true;
	}
	
	/**
	 * Method to add the values from invoice items to the invoice array
	 *
	 * @access private
	 * @return void
	 * @since 1.5
	 */
	function  _insert_values()
	{
		foreach($this->_invoices as &$invoice)
		{
			//add variables to the invoice array that is set initially to 0
			//the variables will store the total price and volumes of all the invoice items
			$invoice->total_price = 0;
			$invoice->total_volume = 0;
			
			foreach($this->_items as $item)
			{
				if($item->invoice == $invoice->id)
				{
					$invoice->total_price += $item->qty * $item->unit_price;
					$invoice->total_volume += $item->qty * $item->volume;
				}
			}
			
			//using the total amounts, calculate the different code amounts
			$invoice->bank_price 	= round(($invoice->bank_percentage / 100 * $invoice->total_price), 2);
			$invoice->coop_price 	= round((2 / 9 * $invoice->total_volume), 2);
			$invoice->winery_price 	= round(($invoice->total_price - $invoice->bank_price - $invoice->coop_price), 2);
		}
		unset($invoice);
		
		$this->_data = $this->_invoices;
	}
}
