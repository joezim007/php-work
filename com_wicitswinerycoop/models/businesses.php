<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelBusinesses extends JModel
{
	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData(&$user)
	{
		if ($user) {
			$where = $user->isAdmin ? '' : 'WHERE winery = '. $this->_db->Quote($user->winery);
			// Load the partner data
			if (!$this->_data)
			{
				$query = 'SELECT * '
						.'FROM #__wicitswinerycoop_businesses '
						.$where;
						
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObjectList();
			}

			return $this->_data;
		}
		return null;
	}
}
