<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelWinery extends JModel
{
	/**
	 * Partner id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		$this->setId(JRequest::getVar('id', 0));
		parent::__construct();
	}

	/**
	 * Method to set the partner identifier
	 *
	 * @access	public
	 * @param	int Partner identifier
	 */
	function setId($id)
	{
		// Set partner id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		// Load the partner data
		if (!$this->_loadData())
		{
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Method to store the partner
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function store($data)
	{
		$name = trim($data['name']);
		$abbr = trim($data['abbr']);
		$id   = isset($data['id']) ? trim($data['id']) : 0;
		
		// Edit Current User 
		if ($id) {
			$query = ' UPDATE #__wicitswinerycoop_wineries '
					.' SET name = ' . $this->_db->Quote($name)
					.', abbr = ' .    $this->_db->Quote($abbr)
					.' WHERE id = ' . $this->_db->Quote($id);
		}
		// New User 
		else {
			$query = 'INSERT INTO #__wicitswinerycoop_wineries '
					.'(name, abbr) '
					.'VALUES (' . $this->_db->Quote($name) . ', '
					. $this->_db->Quote($abbr) . ')';
		}
		
		$this->_db->setQuery($query);
		
		if ( !$this->_db->query() ) {			
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		return true;
	}

	/**
	 * Method to load content partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			if ($this->_id) {				
				$query = 'SELECT * '
						.'FROM #__wicitswinerycoop_wineries '
						.'WHERE id = '. $this->_db->Quote($this->_id)
						.' LIMIT 1';
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObject();
				
				return (boolean) $this->_data;
			}
			
			return false;
		}
		return true;
	}

	/**
	 * Method to initialise the partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$data = new stdClass();
			$data->id    = 0;
			$data->name	 = null;
			$data->abbr	 = null;
			$this->_data = $data;
			return (boolean) $this->_data;
		}
		return true;
	}
}
