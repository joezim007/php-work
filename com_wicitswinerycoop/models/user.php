<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Partners Component Partner Model
 *
 * @package		Joomla
 * @subpackage	Partners
 * @since 1.5
 */
class WineryModelUser extends JModel
{
	/**
	 * Partner id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * Partner data
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		$this->setId(JRequest::getVar('id', 0));
		parent::__construct();
	}

	/**
	 * Method to set the partner identifier
	 *
	 * @access	public
	 * @param	int Partner identifier
	 */
	function setId($id)
	{
		// Set partner id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a partner
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		// Load the partner data
		if (!$this->_loadData())
		{
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Method to store the partner
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function store($data)
	{
		$name       = trim($data['name']);
		$username   = trim($data['username']);
		$password1  = trim($data['password1']);
		$password2  = trim($data['password2']);
		$winery     = isset($data['winery']) ? trim($data['winery']) : null;
		$access     = isset($data['access']) ? trim($data['access']) : null;
		$id         = isset($data['id'])     ? trim($data['id'])     : 0;
		
		if ($password1 != $password2) {
			$this->setError('The passwords you selected do not match');
			return false;
		}
		
		// Edit Current User 
		if ($id) {
			$query = 'UPDATE #__wicitswinerycoop_users '
					.'SET name = ' . $this->_db->Quote($name)
					.', username = ' . $this->_db->Quote($username);
					
			if ($password1 != '') {
				$query .= ', password = MD5(' . $this->_db->Quote($password1) . ') ';
			}
			
			if ($winery && $access != null) {
				$query .= ', winery   = ' . $this->_db->Quote($winery)
						. ', access   = ' . $this->_db->Quote($access);
			}
			
			$query .= 'WHERE id = ' . $this->_db->Quote($id);
		}
		// New User 
		else {		
			if ($winery === 0 && $access > 0 ) {
				$winery = 1;
			}
			
			$query = 'INSERT INTO #__wicitswinerycoop_users '
					.'(name, username, password, winery, access) '
					.'VALUES (' . $this->_db->Quote($name) . ', '
					. $this->_db->Quote($username) . ', '
					.'MD5(' . $this->_db->Quote($password1) . '), '
					. $this->_db->Quote($winery) . ', '
					. $this->_db->Quote($access) . ')';
		}
		
		$this->_db->setQuery($query);
		
		if ( !$this->_db->query() ) {			
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		return true;
	}

	/**
	 * Method to load content partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			if ($this->_id) {				
				$query = 'SELECT u.id, u.winery, u.username, u.name, u.access,  w.abbr, w.name AS winery_name'.
						' FROM #__wicitswinerycoop_users u,' .
						'      #__wicitswinerycoop_wineries w' .
						' WHERE u.id = '. $this->_db->Quote($this->_id) .
						' AND u.winery = w.id' .
						' LIMIT 1';
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObject();
			}
			else {
				$user = JRequest::getString('username', '');
				$pass = JRequest::getString('password', '');
				
				$query = 'SELECT u.id, u.winery, u.username, u.name, u.access,  w.abbr, w.name AS winery_name'.
						' FROM #__wicitswinerycoop_users u,' .
						'      #__wicitswinerycoop_wineries w' .
						' WHERE username = '. $this->_db->Quote($user) .
						' AND password = MD5('. $this->_db->Quote($pass) .')' .
						' AND u.winery = w.id'.
						' LIMIT 1';
				$this->_db->setQuery($query);
				$this->_data = $this->_db->loadObject();
			}
			return (boolean) $this->_data;
		}
		return true;
	}

	/**
	 * Method to initialise the partner data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$user = new stdClass();
			$user->id		= 0;
			$user->winery	= 0;
			$user->username	= null;
			$user->password	= null;
			$user->name		= null;
			$user->access	= 0;
			$this->_data	= $user;
			return (boolean) $this->_data;
		}
		return true;
	}
}
