<?php
	$session =& JFactory::getSession();
	$user = $session->get('winery_user', null);
?>
<link rel="stylesheet" type="text/css" media="screen" href="components/com_wicitswinerycoop/css/winery_header.css" />
<div id="winery_header">
	<h4>Winery Co-op Quick Links</h4>
	<ul class="winery_head_menu">
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=controlpanel', false)?>">Control Panel</a></li>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoice', false)?>">New Invoice</a></li>
<?php if ($user->isAdmin) : ?>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=user', false)?>">New User</a></li>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=winery', false)?>">New Winery</a></li>
<?php else : ?>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=business', false)?>">New Business</a></li>
<?php endif; ?>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=user&id='.$user->id, false)?>">User Account</a></li>
		<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&task=logout', false)?>">Log Out</a></li>
	</ul>
</div>