<?php

class WineryUser {

	var $username = null;
	var $name     = null;
	var $winery   = null;
	var $abbr     = null;
	var $id       = 0;
	var $access   = 0;
	var $isAdmin  = false;
	var $isBanker = false;
	
	static function create($obj) {
		$user = new WineryUser();
	
		$user->username = $obj->username;
		$user->name     = $obj->name;
		$user->winery   = $obj->winery;
		$user->abbr     = $obj->abbre;
		$user->id       = $obj->id;
		$user->access   = $obj->access;
		
		// These two lines are basically the reason we have a class for this. 
		// This is just so that if we change the implementation, we only have 
		// to make the changes here.
		$user->isAdmin  = $obj->access == 1;
		$user->isBanker = $obj->access == 2;
		
		return $user;
	}

}