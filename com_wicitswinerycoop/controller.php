<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Partners Component Controller
 * 
 * Decides what actions should be done depending on the $task parameter.
 * Display is the default action when no task is specified.
 */
class WineryController extends JController
{
	
	var $_comURL = 'index.php?option=com_wicitswinerycoop';
	var $_defaultView = 'controlpanel';
	var $_winery_user = null;
	
	/**
	 * Default method if no task is specified
	 */
	function display()
	{
		// Set a default view if none exists
		$view = JRequest::getCmd( 'view', null );
		if ( !$view ) {
			$view = $this->_defaultView;
			JRequest::setVar('view', $view );
		}
		
		if ( !$this->_isLoggedIn() && $view != 'userlogin' ) {
			$this->_redirectToLogin();
			return;
		}
		
		if ( $this->_isLoggedIn() ) {
			switch ($view) {
				case 'invoice':
					$view =& $this->getView($view, 'html');
					$view->setModel($this->getModel('wineries'));
					$view->setModel($this->getModel('businesses'));
					break;
					
				case 'controlpanel':
					$view =& $this->getView($view, 'html');
					$view->setModel($this->getModel('invoices'));
					$view->setModel($this->getModel('businesses'));
					$view->setModel($this->getModel('wineries'));
					$view->setModel($this->getModel('users'));
					break;
				
				case 'user':
					$id = JRequest::getVar('id', null);
					if ((!$id && $this->_winery_user->isAdmin) || ($id && ($this->_winery_user->id == $id || $this->_winery_user->isAdmin) )) {
						$view =& $this->getView($view, 'html');
						$view->setModel($this->getModel('wineries'));
					}
					else {		
						$msg  = "You must be an administrator to add users or edit other users.";
						$type = "error";
						
						$this->setRedirect(JRoute::_($this->_comURL, false), $msg, $type);
						return;			
					}
					break;
				
				case 'winery':
					if (!$this->_winery_user->isAdmin) {
						$msg  = "You must be an administrator to add or edit wineries.";
						$type = "error";
						
						$this->setRedirect(JRoute::_($this->_comURL, false), $msg, $type);	
						return;				
					}
					break;
			}
			
		}

		parent::display();
	}
	
	function login() {
		$model =& $this->getModel('user');
		$winery_user =& $model->getData();
		$winery_user->isStandard	= $winery_user->access == 0;
		$winery_user->isAdmin		= $winery_user->access == 1;
		$winery_user->isBanker		= $winery_user->access == 2;
		$winery_user->isSecretary 	= $winery_user->access == 3;
		
		if ( $winery_user->id ) {
			$session =& JFactory::getSession();
			$session->set('winery_user', $winery_user);
			
			$view = JRequest::getVar('view', 'controlpanel');
			$view = $view == 'userlogin' ? '&view=controlpanel' : '&view='.$view;
			
			$url = $this->_comURL.$view/*.$redirect.$itemId*/;
			$msg = "Welcome, ".$winery_user->name."!";
		
			$this->setRedirect(JRoute::_($url, false), $msg);
			return;
		}
		else {			
			$url      = $this->_comURL."&view=userlogin".$redirect.$itemId;
			$msg      = "Username and Password Combination does not exist";
			$type     = "error";
			
			$this->setRedirect(JRoute::_($url, false), $msg, $type);
			return;
		}
	}
	
	function logout() {
		$session =& JFactory::getSession();
		$winery_user = $session->set('winery_user', null);
		
		$url = $this->_comURL.'&view=userlogin';
		$msg = 'You have successfully logged out of the Winery Co-op system';
		$this->setRedirect(JRoute::_($url, false), $msg);
		return;
	}
	
	function payinvoice() {		
		if ( !$this->_isLoggedIn() ) {
			$this->_redirectToLogin();
			return;
		}
		else {
			$model = $this->getModel('invoice');
			$getVars =& JRequest::get('get');
			
			$view = '&view=invoices';
			if($getVars['to'] == 'cp') {
				$view = '';
			}
			
			if ( $model->payInvoice($getVars) ) {
				
				$url = $this->_comURL.$view;
				$msg = 'The paid status has been changed successfully';
				$this->setRedirect(JRoute::_($url, false), $msg);
				return;
			}
			else {			
				$url  = $this->_comURL.$view;
				$msg  = 'Sorry, there was a problem changing the paid status:<br>'.$model->getError();
				$type = 'error';
				$this->setRedirect(JRoute::_($url, false), $msg, $type);
				return;
			}
		}
	}
	
	function saveinvoice () {
		$this->_saveItem('invoice');
	}
	
	function saveuser () {
		$this->_saveItem('user');
	}
	
	function savebusiness () {
		$this->_saveItem('business');
	}
	
	function savewinery () {
		$this->_saveItem('winery');
	}
	
	function _saveItem ($itemType) {
		
		if ( !$this->_isLoggedIn() ) {
			$this->_redirectToLogin();
			return;
		}
		else {
			$post = JRequest::get('post');
			$model = $this->getModel($itemType);
			
			if ( $model->store($post) ) {
				$url = $this->_comURL.'';
				$msg = 'The '.$itemType.' was saved successfully';
				$this->setRedirect(JRoute::_($url, false), $msg);
				return;
			}
			else {
				$id   = $post['id'];
				$id   = $id ? '&id='.$id : '';
				
				if (!$id) {
					$session =& JFactory::getSession();
					$session->set('postData', $post);
				}
			
				$url  = $this->_comURL.'&view='.$itemType.$id;
				$msg  = 'Sorry, there was a problem saving the '.$itemType.':<br>'.$model->getError();
				$type = 'error';
				$this->setRedirect(JRoute::_($url, false), $msg, $type);
				return;
			}
		}
	}
	
	function _getUser() {
		$session =& JFactory::getSession();
		$this->_winery_user = $session->get('winery_user', null);		
	}
	
	function _isLoggedIn() {
		if( !isset($this->_winery_user->id) || !$this->_winery_user->id ) {		
			$this->_getUser();			
			return (isset($this->_winery_user->id) && $this->_winery_user->id);
		}
		return true;
	}
	
	function _redirectToLogin() {
		$url = $this->_comURL.'&view=userlogin';
		$msg = 'You must log in to view this page';
		$this->setRedirect(JRoute::_($url, false), $msg);		
	}
}









