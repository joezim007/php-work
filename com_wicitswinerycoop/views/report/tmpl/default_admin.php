<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Jacob Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');

if($this->user->isAdmin): 
	/************************************************
	******** NOTE: look up the css styling **********
	************************************************/?>
	
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/invoice.css', false) ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/jquery-ui-custom.min.css', false) ?>" />
	
	<!-- Add a box to select the time period the report covers -->
	<table>
	<!-- general look of the table - can tweek as needed!-->
		<tr> 
			<th width="90" align="left">	Invoice # ^	</th> <?php//!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="100" align="left">	Date ^		</th> <?php//!!!!!!!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="150" align="left">	Business ^	</th> <?php//!!!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="50" align="left">	Paid?		</th>
			<th width="50" align="left">	Winery $	</th>
			<th width="50" align="left">	Bank $		</th>
			<th width="50" align="left">	Coop $		</th>
		</tr>
	<?php 	//Get all the invoices from this month - either getting a different 
		//set of information for each winery, or just getting the data 
		//grouped by winery
		
		//total amounts per winery and overall
		$total_winery = 0;
		$total = 0;
		
		//for each winery display following header
		foreach($this->invoices as $winery):
	?>
		<tr>
			<th colspan="5" align="left"> 
			<?php echo $winery[0]->winery_name;//winery name ?>
			</th>
		</tr>
	<?php
			//for each invoice per winery, display following information
			foreach($winery as $invoice):
	?>
		<tr>
			<td>	<b><?php echo $invoice->order_num;//invoice# ?></b>	</td>
			<td>	<?php echo $invoice->date;//date ?>			</td>
			<td>	<?php echo $invoice->bill_to//business?>	</td>
			<td>	<font color="<?php echo $invoice->paid ? 'green' : 'red';//if unpaid, red; if paid, green?>"><?php echo $invoice->paid ? 'Paid' : 'Unpaid'; //paid/unpaid ?></font></td>
			<td>	<?php echo number_format($invoice->winery_price, 2); ?>		</td>
			<td>	<?php echo number_format($invoice->bank_price, 2); ?>	</td>
			<td>	<?php echo number_format($invoice->coop_price, 2); ?>		</td>
		<?php	//add the invoice's total (calculated in the model) to the winery total
				$total_winery += $invoice->total_price; ?>
			
		</tr>
	<?php
			//end for each invoice loop
			endforeach;
	?>
		<tr>
			<td colspan="3" align="right">	<b><?php echo $winery[0]->winery_name;//winery name?> Totals:</b>	</td>
			<td>	<b><?php echo number_format($total_winery, 2); ?></b>	</td>
		</tr>
	<?php
	
		//Add up the new winery total for the overall total before resetting the winery total
		$total += $total_winery;
		//reset the winery total every time it switches wineries.
		$total_winery = 0;
		//end for each winery loop
		endforeach;
		
	?>
		<tr>
			<td colspan="3" align="center">	<b>Grand Totals:</b>	</td>			
			<td>	<b><?php echo number_format($total, 2); ?></b>	</td>
		</tr>
	</table>
<?php endif; ?>