<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Jacob Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

jimport( 'joomla.utilities.date');

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');
	/************************************************
	******** NOTE: look up the css styling **********
	************************************************/?>
	
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/invoice.css', false) ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/jquery-ui-custom.min.css', false) ?>" />
	
	<!-- Add a box to select the time period the report covers !-->
	<table>
	<!-- general look of the table - can tweek as needed!-->
		<tr> 
			<th width="90" align="left">	Invoice # ^	</th> <?php//!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="100" align="left">	Date ^		</th> <?php//!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="150" align="left">	Business ^	</th> <?php//!!!!!!!!!needs a 'sort by' button!!!!!!!!!!!!!!!?>
			<th width="50" align="left">	Paid?		</th>
			<th width="50" align="left">	Winery $	</th>
			<th width="50" align="left">	Bank $		</th>
			<th width="50" align="left">	Coop $		</th>
		</tr>
	<?php 	
		
		//Get totals for all columns
		$winery_total = 0;
		$bank_total = 0;
		$coop_total = 0;
		//Get all the invoices from this month for the winery the user is from
		//for each invoice for this winery, display following information
		foreach($this->invoices as $invoice):
	?>
		<tr>
			<td>	<b><?php echo $invoice->order_num;//invoice# ?></b>	</td>
			<td>	<?php echo $invoice->date; // date ?>	</td>
			<td>	<?php echo $invoice->bill_to;//business?>	</td>
			<td>	<font color="<?php echo $invoice->paid ? 'green' : 'red';//if unpaid, red; if paid, green?>"><?php echo $invoice->paid ? 'Paid' : 'Unpaid'; //paid/unpaid ?></font></td>
			<td>	<?php echo number_format($invoice->winery_price, 2); $winery_total += $invoice->winery_price; ?>		</td>
			<td>	<?php echo number_format($invoice->bank_price, 2); $bank_total += $invoice->bank_price; ?>	</td>
			<td>	<?php echo number_format($invoice->coop_price, 2); $coop_total += $invoice->coop_price; ?>	</td>
		</tr>
	<?php
		//end both for each invoice loop
		endforeach;
	?>
		<tr>
			<td colspan="2"> As of 	<?php $date = new JDate(); echo $date->toFormat('%Y-%m-%d');//current date and time here ?>	</td>	<!--THIS NEEDS THE CALCULATION!-->
			<td  align="left">	<b>Totals:</b>	</td>
			<td>	<b><?php echo number_format($invoice->total_price, 2); ?></b>	</td>
			<td>	<?php echo number_format($winery_total, 2); ?>		</td>
			<td>	<?php echo number_format($bank_total, 2); ?>	</td>
			<td>	<?php echo number_format($coop_total, 2); ?>	</td>
		</tr>
	</table>