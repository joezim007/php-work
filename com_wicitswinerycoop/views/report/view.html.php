<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright		Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Jacob Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */

class WineryViewReport extends JView
{
	function display( $tpl = null)
	{		
		$session  = JFactory::getSession();
		$user     = $session->get('winery_user', null);			
		$model    =& $this->getModel('report');
		$invoices =& $model->getData($user);
		$wineries   =& $this->get('data', 'wineries');	
		

		if($user->isAdmin)
		{
			$tpl = 'admin';
			
			//sort the invoice data between wineries, and make
			// it easier to do calculations per winery
			$report_invoices = null;
			foreach ($invoices as $invoice) 
			{
				$report_invoices[$invoice->winery_name][] = $invoice;
			}
			$this->assignRef('invoices',   $report_invoices);
		}		
		else
		{
			$this->assignRef('invoices', $invoices);
		}
				
		$this->assignRef('user',     $user);
		$this->assignRef('wineries',   $wineries);
		
		

		parent::display($tpl);		
	}
}
 
?>