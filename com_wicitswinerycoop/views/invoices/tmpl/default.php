<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

jimport('joomla.utilities.date');

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');
?>
<link rel="stylesheet" type="text/css" href="components/com_wicitswinerycoop/css/invoices.css" />
<div id="winery_invoices">

<h1>All Invoices</h1>
<div class="section">
	<form class="section_head" action="<?php echo JRoute::_('index.php', false) ?>" method="get">
		<h3>Recent Invoices</h3>
		<label for="invoice_search">Invoice Number:</label> <input type="text" id="invoice_search" name="invoice_search" disabled="disabled" value="Not Yet Implemented" /><input type="submit" value="Find" disabled="disabled" />
		<div class="nav">
		<?php if ($this->page > 1) : ?>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoices&pg='.($this->page - 1), false)?>">&lt;&lt; Previous</a>
		<?php endif; ?>
		<?php if ($this->page > 1 && $this->page < $this->pages) : ?>
			|
		<?php endif; ?>
		<?php if ($this->page < $this->pages) : ?>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoices&pg='.($this->page + 1), false)?>">Next&gt;&gt;</a>
		<?php endif; ?>
		</div>
		<input type="hidden" name="option" value="com_wicitswinerycoop" />
		<input type="hidden" name="view" value="invoices" />
	</form>
	<table class="section_body">
<?php 
if ($this->invoices) :
	foreach ($this->invoices as $invoice) :
		$order_date = new JDate($invoice->date);
		$order_date = $order_date->toFormat('%m/%d/%y');
		$edit_date  = new JDate($invoice->created);
		$edit_date  = $edit_date->toFormat('%m/%d/%y');
		
		$paid       = $invoice->paid ? 'Paid' : 'Unpaid';
?>
		<tr>
			<td class="invoice_list_left">
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
					<span class="invoice_num"><?php echo $invoice->winery.$invoice->order_num ?></span>
					<span class="invoice_itemcount">(<?php echo $invoice->numItems ?> Items)</span><br/>
					<span class="invoice_business"><?php echo $invoice->bill_to ?></span><br/>
			<?php if(!$this->user->isBanker) :?>
				</a>
			<?php endif; ?>
			</td>
			<td class="invoice_list_right">
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
					<span class="invoice_date"><?php echo $order_date ?></span>
					<span class="invoice_edit_date">(Edited: <?php echo $edit_date ?>)</span><br/>
			<?php if($this->user->isBanker) :
					$marks = array("paid" => "Mark Unpaid", "unpaid" => "Mark Paid");
			?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&task=payinvoice&id=".$invoice->id, false)?>" class="mark"><?php echo $marks[strtolower($paid)] ?></a>
			<?php endif; ?>
					<span class="invoice_<?php echo strtolower($paid); ?>"><?php echo $paid ?></span>
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
			</td>
		</tr>
<?php 
	endforeach; 
else :
?>
		<tr><td style="text-align:center;">There are no saved invoices.</td></tr>
<?php endif; ?>
	</tbody>
	</table>
</div>
</div>