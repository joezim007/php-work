<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */
class WineryViewInvoices extends JView
{
	function display( $tpl = null)
	{
		$session  =& JFactory::getSession();
		$user     =& $session->get('winery_user', null);
		$page     =  JRequest::getInt( 'pg', 0 );
		if (!$page) {
			JRequest::setVar('pg', 1);
			$page = 1;
		}
		
		$model    =& $this->getModel('invoices');
		$invoices =& $model->getData($user);
		$pages    =& $model->getPages($user);
		
		$this->assignRef('invoices',   $invoices);
		$this->assignRef('pages',      $pages);
		$this->assignRef('page',       $page);
		$this->assignRef('user',       $user);
		
		parent::display($tpl);
	}
}
?>
