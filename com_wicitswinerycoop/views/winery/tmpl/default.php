<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');

if($this->isNew) :
?>
<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
	<h2>Add A Winery</h2>
	<div>
		<label for="name">Winery Name:</label>
		<input type="text" name="name" id="name" value="<?php if($this->data) echo $this->data['name'] ?>" />
	</div>
	<div>
		<label for="abbr">Abbreviation:</label>
		<input type="text" name="abbr" id="abbr" value="<?php if($this->data) echo $this->data['abbr'] ?>"/>
	</div>
	<div class="controls">
		<input type="submit" value="Submit Winery" />
	</div>
	<input type="hidden" name="option" value="com_wicitswinerycoop" />
	<input type="hidden" name="task" value="savewinery" />
</form>

<?php elseif ($this->winery->id) :?>
<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
	<h2>Add A Winery</h2>
	<div>
		<label for="name">Winery Name:</label>
		<input type="text" name="name" id="name" value="<?php echo $this->winery->name ?>" />
	</div>
	<div>
		<label for="abbr">Abbreviation:</label>
		<input type="text" name="abbr" id="abbr" value="<?php echo $this->winery->abbr ?>" />
	</div>
	<div class="controls">
		<input type="submit" value="Submit Winery" />
	</div>
	<input type="hidden" name="option" value="com_wicitswinerycoop" />
	<input type="hidden" name="task" value="savewinery" />
	<input type="hidden" name="id" value="<?php echo $this->winery->id ?>" />
</form>
<?php else :?>
	<div style="background-color:#FEF1EC; border:1px solid red; color:red; padding:15px;">
		<h2>Error Retrieving Winery</h2>
		<p>
			Sorry, we could not find an winery matching the specified ID. Either the winery was deleted or the URL is wrong or broken.
		</p>
	</div>
<?php endif;?>