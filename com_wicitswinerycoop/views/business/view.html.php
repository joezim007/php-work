<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */
class WineryViewBusiness extends JView
{
	function display( $tpl = null)
	{
		$session  = JFactory::getSession();
		$user     = $session->get('winery_user', null);	
		
		$id       = JRequest::getVar('id', null);
		$isNew    = !$id;
		
		if (!$isNew) {
			$business =& $this->get('data', 'business');
			
			$this->assign('business', $business);
		}
		else {
			$data = $session->get('postData', null);
			$session->set('postData', null);
			
			$this->assignRef('data', $data);
		}
		
		$this->assignRef('user',     $user);
		$this->assignRef('isNew',    $isNew);
		
		parent::display($tpl);
	}
}
?>
