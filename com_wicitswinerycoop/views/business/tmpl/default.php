<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');

if($this->isNew) :
?>
<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
	<h2>Add A Business</h2>
	<div>
		<label for="name">Business Name:</label>
		<input type="text" name="name" id="name" value="<?php if($this->data) echo $this->data['name'] ?>" />
	</div>
	<div>
		<label for="address">Address:</label>
		<input type="text" name="address" id="address" value="<?php if($this->data) echo $this->data['address'] ?>"/>
	</div>
	<div>
		<label for="address2">City, State, Zip:</label>
		<input type="text" id="address2" name="address2" value="<?php if($this->data) echo $this->data['address2'] ?>"/>
	</div>
	<div>
		<label for="reseller_num">Reseller Number:</label>
		<input type="text" id="reseller_num" name="reseller_num" value="<?php if($this->data) echo $this->data['reseller_num'] ?>"/>
	</div>
	<div class="controls">
		<input type="submit" value="Submit Business" />
	</div>
	<input type="hidden" name="option" value="com_wicitswinerycoop" />
	<input type="hidden" name="task" value="savebusiness" />
	<input type="hidden" name="winery" value="<?php echo $this->user->winery ?>" />
</form>

<?php elseif ($this->business->id) :?>
<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
	<h2>Edit a Business</h2>
	<div>
		<label for="name">Business Name:</label>
		<input type="text" name="name" id="name" value="<?php echo $this->business->name ?>" />
	</div>
	<div>
		<label for="address">Address:</label>
		<input type="text" name="address" id="address" value="<?php echo $this->business->address ?>" />
	</div>
	<div>
		<label for="address2">City, State, Zip:</label>
		<input type="text" id="address2" name="address2" value="<?php echo $this->business->address2 ?>" />
	</div>
	<div>
		<label for="reseller_num">Reseller Number:</label>
		<input type="text" id="reseller_num" name="reseller_num" value="<?php echo $this->business->reseller_num ?>" />
	</div>
	<div class="controls">
		<input type="submit" value="Submit Business" />
	</div>
	<input type="hidden" name="option" value="com_wicitswinerycoop" />
	<input type="hidden" name="task" value="savebusiness" />
	<input type="hidden" name="winery" value="<?php echo $this->user->winery ?>" />
	<input type="hidden" name="id" value="<?php echo $this->business->id ?>" />
</form>
<?php else :?>
<div style="background-color:#FEF1EC; border:1px solid red; color:red; padding:15px;">
	<h2>Error Retrieving Business</h2>
	<p>
		Sorry, we could not find an business matching the specified ID. Either the business was deleted or the URL is wrong or broken.
	</p>
</div>
<?php endif;?>