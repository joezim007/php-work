<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */
class WineryViewControlpanel extends JView
{
	function display( $tpl = null)
	{
		$user =& $this->getWineryUser();
		
		if ($user) {			
			$model      =& $this->getModel('invoices');
			$invoices   =& $model->getData($user);	
			$model      =& $this->getModel('businesses');
			$businesses =& $model->getData($user);	
			$wineries   =& $this->get('data', 'wineries');
			$users      =& $this->get('data', 'users');
			
			$this->assignRef('invoices',   $invoices);
			$this->assignRef('businesses', $businesses);
			$this->assignRef('wineries',   $wineries);
			$this->assignRef('users',      $users);
			$this->assignRef('user',       $user);
		}
		
		parent::display($tpl);
	}
	
	function &getWineryUser() 
	{
		$session =& JFactory::getSession();
		return $session->get('winery_user', null);
	}
}
?>
