<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

jimport('joomla.utilities.date');
?>
<link rel="stylesheet" type="text/css" href="components/com_wicitswinerycoop/css/controlpanel.css" />
<div id="winery_control_panel">

<h1>Winery Co-op Control Panel</h1>
<!-- BEGIN INVOICES -->
<div id="winery_recent_invoices" class="section">
	<div class="section_head">
		<h3>Recent Invoices</h3>
		<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoices', false)?>">View All</a>
<?php if ($this->user->isStandard) : ?>
		| <a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoice', false)?>">New Invoice</a>
<?php endif; ?>
	</div>
	<table class="section_body">
<?php 
if ($this->invoices) :
	foreach ($this->invoices as $invoice) :
		$order_date = new JDate($invoice->date);
		$order_date = $order_date->toFormat('%m/%d/%y');
		$edit_date  = new JDate($invoice->created);
		$edit_date  = $edit_date->toFormat('%m/%d/%y');
		
		$paid       = $invoice->paid ? 'Paid' : 'Unpaid';
?>
		<tr>
			<td class="invoice_list_left">
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
					<span class="invoice_num"><?php echo $invoice->winery.$invoice->order_num ?></span>
					<span class="invoice_itemcount">(<?php echo $invoice->numItems ?> Items)</span><br/>
					<span class="invoice_business"><?php echo $invoice->bill_to ?></span><br/>
			<?php if(!$this->user->isBanker) :?>
				</a>
			<?php endif; ?>
			</td>
			<td class="invoice_list_right">
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
					<span class="invoice_date"><?php echo $order_date ?></span>
					<span class="invoice_edit_date">(Edited: <?php echo $edit_date ?>)</span><br/>
			<?php if($this->user->isBanker) :
					$marks = array("paid" => "Mark Unpaid", "unpaid" => "Mark Paid");
			?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&to=cp&task=payinvoice&id=".$invoice->id, false)?>" class="mark"><?php echo $marks[strtolower($paid)] ?></a>
			<?php endif; ?>
					<span class="invoice_<?php echo strtolower($paid); ?>"><?php echo $paid ?></span>
			<?php if(!$this->user->isBanker) :?>
				<a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice&id=".$invoice->id, false)?>">
			<?php endif; ?>
			</td>
		</tr>
<?php 
	endforeach;
else :
?>
		<tr><td style="text-align:center;">There are no saved invoices.</td></tr>
<?php endif; ?>
	</tbody>
	</table>
</div>
<!-- END INVOICES -->
<!-- BEGIN QUICK LINKS -->
<div class="sidebar">
	<div id="winery_quick_links" class="section">
		<div class="section_head">
			<h3>Quick Links</h3>
		</div>
		<ul class="section_body">
		<?php if(!$this->user->isBanker) :?>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoice", false)?>">New Invoice</a></li>
		<?php else: ?>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=invoices", false)?>">All Invoices</a></li>
		<?php endif;?>		
		<?php if(!$this->user->isAdmin && !$this->user->isBanker) :?>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=business", false)?>">New Business</a></li>
		<?php elseif($this->user->isAdmin) : ?>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=winery", false)?>">New Winery</a></li>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=user", false)?>">New User</a></li>
		<?php endif; ?>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=report", false)?>">View Report</a></li>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&view=user&id=".$this->user->id, false)?>">Edit Your User Account</a></li>
			<li><a href="<?php echo JRoute::_("index.php?option=com_wicitswinerycoop&task=logout", false)?>">Log Out</a></li>
		</ul>
	</div>
<!-- END QUICK LINKS -->
<?php if (!$this->user->isAdmin && !$this->user->isBanker) : ?>
<!-- BEGIN BUSINESSES -->
	<div id="winery_businesses" class="section">
		<div class="section_head">
			<h3>Businesses</h3>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=business',false)?>">New Business</a>
		</div>
		<ul class="section_body">
<?php
if ($this->businesses) :
	foreach ($this->businesses as $business) :
?>
			<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=business&id='.$business->id, false); ?>"><?php echo $business->name; ?></a></li>
<?php 
	endforeach;
else :
?>
			<li style="text-align:center">There are no businesses</li>
<?php endif; ?>
		</ul>
	</div>
<!-- END BUSINESSES -->
<?php endif; ?>
<?php if ($this->user->isAdmin) : ?>
<!-- BEGIN WINERIES -->
	<div id="winery_wineries" class="section">
		<div class="section_head">
			<h3>Wineries</h3>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=winery',false)?>">New Winery</a>
		</div>
		<ul class="section_body">
<?php
if ($this->wineries) :
	foreach ($this->wineries as $winery) :
?>
			<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=winery&id='.$winery->id, false); ?>"><?php echo $winery->name; ?></a></li>
<?php 
	endforeach;
else :
?>
			<li style="text-align:center">There are no wineries</li>
<?php endif; ?>
		</ul>
	</div>
<!-- END WINERIES -->
<!-- BEGIN USERS -->
	<div id="winery_users" class="section">
		<div class="section_head">
			<h3>Users</h3>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=user',false)?>">New User</a>
		</div>
		<ul class="section_body">
<?php
if ($this->users) :
	foreach ($this->users as $user) :
?>
			<li><a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=user&id='.$user->id, false); ?>"><?php echo $user->name; ?></a></li>
<?php 
	endforeach;
else :
?>
			<li style="text-align:center">There are no users</li>
<?php endif; ?>
		</ul>
	</div>
<!-- END USERS -->
<?php endif; ?>
</div>

</div>