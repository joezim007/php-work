<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');

if($this->isNew) : ?>
	<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
	<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
		<h2>Add A User</h2>
		<div>
			<label for="name">Name:</label>
			<input type="text" name="name" id="name" value="<?php if($this->data) echo $this->data['name'] ?>" />
		</div>
		<div>
			<label for="username">User Name:</label>
			<input type="text" name="username" id="username" autocomplete="off" value="<?php if($this->data) echo $this->data['username'] ?>" />
		</div>
		<div>
			<label for="password1">Password:</label>
			<input type="password" id="password1" name="password1" autocomplete="off" value="<?php if($this->data) echo $this->data['password1'] ?>" />
		</div>
		<div>
			<label for="password2">Password Again:</label>
			<input type="password" id="password2" name="password2" autocomplete="off" value="<?php if($this->data) echo $this->data['password2'] ?>" />
		</div>
		<div>
			<label for="winery">Winery:</label>
			<select name="winery" id="winery">
				<option value="0">-Select-</option>
		<?php
			foreach($this->wineries as $winery) :
		?>
				<option value="<?php echo $winery->id?>" <?php if($this->data && $this->data['winery'] == $winery->id) echo 'selected="selected"';?>><?php echo $winery->name?></option>
		<?php endforeach;?>
			</select>
		</div>
		<div>
			<label for="access">Access:</label>
			<select id="access" name="access">
				<option value="0" <?php if($this->data && $this->data['access'] == '0') echo 'selected="selected"';?>>User</option>
				<option value="1" <?php if($this->data && $this->data['access'] == '1') echo 'selected="selected"';?>>Administrator</option>
				<option value="2" <?php if($this->data && $this->data['access'] == '2') echo 'selected="selected"';?>>Banker</option>
			</select>
		</div>
		<div class="controls">
			<input type="submit" value="Submit User" />
		</div>
		<input type="hidden" name="option" value="com_wicitswinerycoop" />
		<input type="hidden" name="task" value="saveuser" />
	</form>

<?php elseif ($this->editedUser->id) :?>

	<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
	<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
		<h2>Edit A User</h2>
		<div>
			<label for="name">Name:</label>
			<input type="text" name="name" id="name" value="<?php echo $this->editedUser->name ?>" />
		</div>
		<div>
			<label for="username">User Name:</label>
			<input type="text" name="username" id="username" autocomplete="off" value="<?php echo $this->editedUser->username ?>" />
		</div>
		<div>
			<label for="password1">New Password:</label>
			<input type="password" id="password1" name="password1" autocomplete="off" value="" />
		</div>
		<div>
			<label for="password2">New Password Again:</label>
			<input type="password" id="password2" name="password2" autocomplete="off" value="" />
		</div>
	<?php if ($this->viewingUser->isAdmin) : ?>
		<div>
			<label for="winery">Winery:</label>
			<select name="winery" id="winery">
				<option value="0">-Select-</option>
		<?php
			foreach($this->wineries as $winery) :
				$selected = $winery->id == $this->editedUser->winery ? '" selected="selected' : '';
		?>
				<option value="<?php echo $winery->id.$selected?>"><?php echo $winery->name?></option>
		<?php endforeach;?>
			</select>
		</div>
		<div>
			<label for="access">Access:</label>
			<select id="access" name="access">
				<option value="0" <?php echo $this->editedUser->access == 0 ? 'selected="selected"' : '' ?>>User</option>
				<option value="1" <?php echo $this->editedUser->access == 1 ? 'selected="selected"' : '' ?>>Administrator</option>
				<option value="2" <?php echo $this->editedUser->access == 2 ? 'selected="selected"' : '' ?>>Banker</option>
			</select>
		</div>
	<?php endif; ?>
		<div class="controls">
			<input type="submit" value="Submit Update" />
		</div>
		<input type="hidden" name="option" value="com_wicitswinerycoop" />
		<input type="hidden" name="task" value="saveuser" />
		<input type="hidden" name="id" value="<?php echo $this->editedUser->id;?>" />
	</form>
<?php else :?>
	<div style="background-color:#FEF1EC; border:1px solid red; color:red; padding:15px;">
		<h2>Error Retrieving User</h2>
		<p>
			Sorry, we could not find an user matching the specified ID. Either the user was deleted or the URL is wrong or broken.
		</p>
	</div>
<?php endif;?>