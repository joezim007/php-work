<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */
class WineryViewUserlogin extends JView
{
	function display( $tpl = null)
	{
		$session =& JFactory::getSession();
		$isLoggedIn = (boolean) ($session->get('winery_user', null));
		
		$this->assign('isLoggedIn', $isLoggedIn);
		
		parent::display($tpl);
	}
}
?>
