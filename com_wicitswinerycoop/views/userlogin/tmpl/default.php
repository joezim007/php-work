<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

if ($this->isLoggedIn) :
?>
<dl id="system-message">
<dt class="error">Error</dt>
<dd class="error message fade">
	<ul>
		<li>
			You are already logged in. You cannot log in again until you 
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&task=logout', false)?>">log out.</a>
		</li>
	</ul>
</dd>
</dl>


<?php else : ?>
<link rel="stylesheet" type="text/css" media="screen, print" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/standard_form.css', false)?>" />
<form id="wineryform" action="<?php echo JRoute::_('index.php', false)?>" method="post">
	<h2>Log Into the Winery Co-op</h2>
	<div>
		<label for="username">User Name:</label>
		<input type="text" name="username" id="username" />
	</div>
	<div>
		<label for="password">Password:</label>
		<input type="password" id="password" name="password" />
	</div>
	<div class="controls">
		<input type="submit" value="Log In" />
	</div>
	<input type="hidden" name="option" value="com_wicitswinerycoop" />
	<input type="hidden" name="task" value="login" />
</form>
<?php endif; ?>