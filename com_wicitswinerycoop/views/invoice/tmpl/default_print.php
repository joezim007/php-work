<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

if ($this->invoice && $this->invoice->id):
?>

<link rel="stylesheet" type="text/css" media="screen, print" href="components/com_wicitswinerycoop/css/invoice.print.css" />
<div id="invoice_form"><!--form action="index.php" method="post" id="invoice_form"-->
	<div class="form_header">
		<h2>Badger State Winery Cooperative</h2>
		<div>
			<label for="date">Date:</label>
			<input type="text" disabled="disabled" value="<?php echo $this->invoice->date; ?>"/>			
			<label for="invoicenum">Invoice #:</label>
			<input type="text" id="invoicenum" disabled="disabled" value="<?php echo $this->invoice->winery.$this->invoice->order_num; ?>" />
		</div>
	</div>
	<div class="invoice_info">
		<h3>Invoice</h3>
		<div class="billing">
			<div>
				<label for="billto">Bill To:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->bill_to; ?>" />
			</div>
			<div>
				<label for="address">Address:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->address; ?>" />
			</div>
			<div>
				<label for="csz">City, State, Zip:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->address2; ?>" />
			</div>
			<div>
				<label for="resellernum">Reseller's Number:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->reseller_num; ?>" />
			</div>
		</div>
		<div class="winery">
			<div>
				<label for="winery">Winery Name:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->wineryname ?>" />
			</div>
			<div>
				<label for="phone">Phone:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->phone; ?>" />
			</div>
			<div>
				<label for="ordernum">Order Number:</label>
				<input type="text" disabled="disabled" value="<?php echo $this->invoice->order_num; ?>" />
			</div>
		</div>
	</div>
	<table class="order_table">
		<thead>
			<tr>
				<th class="th_q">Qty</th>
				<th class="th_v">Volume</th>
				<th class="th_d">Description</th>
				<th class="th_c">Beverage Class</th>
				<th class="th_p">Unit Price</th>
				<th class="th_t">Total</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$i = 0;
	$total = 0;
	foreach ($this->invoice->items as $item) :
		$itemTotal = $item->unit_price * $item->qty;
		$total += $itemTotal;
?>
			<tr class="<?php echo $i == 0 ? 'even' : 'odd'?> ">
				<td><?php echo $item->qty ?></td>
				<td><?php echo $item->volumeString ?></td>
				<td><?php echo $item->description ?></td>
				<td><?php echo $item->bevString ?></td>
				<td>$<?php echo number_format($item->unit_price, 2) ?></td>
				<td>$<?php echo number_format($itemTotal, 2) ?></td>
			</tr>
<?php 
		$i = 1 - $i;
	endforeach;
?>
		</tbody>
		<tfoot>
			<tr>
				<td class="empty" colspan="3"> </td>
				<th colspan="2">Total Due</th>
				<td>$<?php echo number_format($total, 2) ?></td>
			</tr>
		</tfoot>
	</table>
<div id="footer">
	<p class="notes">
		<span>Reminder:</span> Please include the invoice number on your check.<br/>
		<span>Terms:</span> Balance due in 14 days.
	</p>
	<p class="cut">
		<span class="left">Cut here, bottom portion MUST be enclosed with payment</span>
		<span class="right">Thank you!</span>
	</p>
	<p class="address">
		<span>Make check or money order payable to:</span><br/>
		Badger State Winery Cooperative<br/>
		<span>Mail to:</span><br/>
		Benton State Bank<br/>
		42 W. Main PO Box 27<br/>
		Benton, WI 53803
	</p>
	<p class="fold">
		Fold here for windowed envelope
	</p>
	<table class="remittance">
		<tr>
			<th colspan="2">Remittance</th>
		</tr>
		<tr>
			<td class="label">Customer Name:</td>
			<td><?php echo $this->invoice->bill_to; ?></td>
		</tr>
		<tr>
			<td class="label">Customer Reseller #:</td>
			<td><?php echo $this->invoice->reseller_num; ?></td>
		</tr>
		<tr>
			<td class="label">Invoice #:</td>
			<td><?php echo $this->invoice->winery.$this->invoice->order_num; ?></td>
		</tr>
		<tr>
			<td class="label">Invoice Date:</td>
			<td><?php echo $this->invoice->date; ?></td>
		</tr>
		<tr>
			<td class="label strong">Amount Due:</td>
			<td class="strong">$<?php echo number_format($total, 2) ?></td>
		</tr>
		<tr>
			<td class="label">Amount Enclosed:</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Check #:</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
</div>
<?php
else :
?>
<div style="background-color:#FEF1EC; border:1px solid red; color:red; padding:15px;">
	<h2>Error Retrieving Invoice</h2>
	<p>
		Sorry, we could not find an invoice matching the specified ID. Either the invoice was deleted or you typed the URL in wrong.
	</p>
</div>
<?php
endif;
?>