<?php
/**
 * @package		Wi-CITS Winery Co-op
 * @version		0.1 alpha
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); 

include_once (JPATH_COMPONENT.DS.'includes'.DS.'header.php');


if ($this->isNew) :
?>

	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/invoice.css', false) ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/jquery-ui-custom.min.css', false) ?>" />

	<form action="index.php" method="post" id="invoice_form">
		<div class="form_header">
			<h2>Badger State Winery Cooperative</h2>
			<div>
				<label for="date">Date:</label>
				<input type="text" id="date" name="date" value="<?php if($this->data)echo $this->data['date']; ?>"/>			
				<label for="invoicenum">Invoice #:</label>
				<input type="text" id="invoicenum" name="invoicenum" class="autofill" disabled="disabled" value="<?php if($this->data)echo $this->data['winery'].$this->data['ordernum']; ?>" />
			</div>
		</div>
		<div class="invoice_info">
			<h3>Invoice</h3>
			<div class="billing">
				<div>
					<label for="billto">Bill To:</label>
					<input type="text" id="billto" name="billto" value="<?php if($this->data)echo $this->data['billto']; ?>" />
					<ul id="billto_list">
	<?php foreach($this->businesses as $biz) : ?>
						<li><span><?php echo $biz->name ?></span>
							<ul>
								<li class="biz_address"><?php echo $biz->address ?></li>
								<li class="biz_csz"><?php echo $biz->address2 ?></li>
								<li class="biz_resell"><?php echo $biz->reseller_num ?></li>
							</ul>
						</li>
	<?php endforeach; ?>
					</ul>
				</div>
				<div>
					<label for="address">Address:</label>
					<input type="text" id="address" name="address" value="<?php if($this->data)echo $this->data['address']; ?>" />
				</div>
				<div>
					<label for="csz">City, State, Zip:</label>
					<input type="text" id="csz" name="csz" value="<?php if($this->data)echo $this->data['csz']; ?>" />
				</div>
				<div>
					<label for="resellernum">Reseller's Number:</label>
					<input type="text" id="resellernum" name="resellernum" value="<?php if($this->data)echo $this->data['resellernum']; ?>" />
				</div>
			</div>
			<div class="winery">
				<div>
					<label for="winery">Winery Name:</label>
	<?php	if($this->user->isAdmin):?>		
					<select name="winery" id="winery">
						<option value="0">-Select-</option>
	<?php
		foreach($this->wineries as $winery) :
			$selected = $winery->abbr == $this->data['winery'] ? '" selected="selected' : '';
	?>
						<option value="<?php echo $winery->abbr.$selected?>"><?php echo $winery->name?></option>
	<?php endforeach;?>
					</select>
	<?php else: ?>
					<input type="text" disabled="disabled" value="<?php echo $this->user->winery_name ?>" />
					<input type="hidden" value="<?php echo $this->user->abbr ?>" name="winery" id="winery" />
	<?php endif; ?>
				</div>
				<div>
					<label for="phone">Phone:</label>
					<input type="text" id="phone" name="phone" value="<?php if($this->data)echo $this->data['phone']; ?>" />
				</div>
				<div>
					<label for="ordernum">Order Number:</label>
					<input type="text" id="ordernum" name="ordernum" value="<?php if($this->data)echo $this->data['ordernum']; ?>" />
				</div>
			</div>
		</div>
		<div class="controls">
			<input type="button" id="del_row" value="Remove Item(s)" />
			<input type="button" id="add_row" value="Add Item" />
		</div>
		<table class="order_table">
			<thead>
				<tr>
					<th class="th_i"><input type="checkbox" id="select_all" /></th>
					<th class="th_q">Qty</th>
					<th class="th_v">Volume</th>
					<th class="th_d">Description</th>
					<th class="th_c">Beverage Class</th>
					<th class="th_p">Unit Price</th>
					<th class="th_t">Total</th>
				</tr>
			</thead>
			<tbody>
	<?php 
		if ($this->data) :
			$length = count($this->data['quantity']);
			for ($i=0; $i<$length; $i++) :
	?>
				<tr>
					<td><input type="checkbox" name="row<?php echo $i ?>" /></td>
					<td><input type="text" name="quantity[<?php echo $i ?>]" class="int" value="<?php echo $this->data['quantity'][$i] ?>" /></td>
					<td>
						<select name="volume[<?php echo $i ?>]">
							<option value="0">-Select-</option>
							<option value="0.750"<?php if($this->data['volume'][$i] == .75) echo ' selected="selected"';?>>750mL</option>
							<option value="0.375"<?php if($this->data['volume'][$i] == .375) echo ' selected="selected"';?>>375mL</option>
							<option value="0.187"<?php if($this->data['volume'][$i] == .187) echo ' selected="selected"';?>>187mL</option>
							<option value="0.355"<?php if($this->data['volume'][$i] == .355) echo ' selected="selected"';?>>12oz</option>
							<option value="0.473"<?php if($this->data['volume'][$i] == .473) echo ' selected="selected"';?>>16oz</option>
						</select>
					</td>
					<td><input type="text" name="description[<?php echo $i ?>]" value="<?php echo $this->data['description'][$i] ?>" /></td>
					<td>
						<select name="bevclass[<?php echo $i ?>]">
							<option value="0">-Select-</option>
							<option value="1"<?php if($this->data['bevclass'][$i] == 1) echo ' selected="selected"';?>>Apple Cider 7% or less</option>
							<option value="2"<?php if($this->data['bevclass'][$i] == 2) echo ' selected="selected"';?>>Wine 14% or less</option>
							<option value="3"<?php if($this->data['bevclass'][$i] == 3) echo ' selected="selected"';?>>Wine over 14%</option>
						</select>
					</td>
					<td><input type="text" name="unitprice[<?php echo $i ?>]" class="num" value="<?php echo number_format($this->data['unitprice'][$i], 2) ?>" /></td>
					<td><input type="text" name="itemtotal[<?php echo $i ?>]" class="autofill" disabled="disabled" value="<?php echo number_format($this->data['unitprice'][$i] * $this->data['quantity'][$i], 2) ?>" /></td>
				</tr>
	<?php 
			endfor;
		endif;
	?>
			</tbody>
			<tfoot>
				<tr>
					<td class="empty" colspan="4"> </td>
					<th colspan="2">Total Due</th>
					<td><input type="text" name="subtotal" class="autofill" disabled="disabled" value="0.00"/></td>
				</tr>
			</tfoot>
		</table>
		<div class="controls">
			<input type="submit" value="Save Invoice" id="invoice_submit"/>
		</div>
		<input type="hidden" name="option" value="com_wicitswinerycoop" />
		<input type="hidden" name="task" value="saveinvoice" />
	</form>
	<div class="modal">
		<h3>Error: Invalid Fields</h3>
		<p>
			There were some errors found in your form. The fields that need to be changed are highlighted. 
			Hover your cursor over the field for more details. Please resolve all of the issues before
			trying to submit this form again.
		</p>
		<a href="#" class="simplemodal-close close-button" title="Close Dialog Box">X</a>
	</div>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/jquery.wtooltip.min.js', false) ?>"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/jquery.simplemodal.min.js', false) ?>"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/invoice.js', false) ?>"></script>

<?php elseif ($this->invoice->id) : ?>

	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/invoice.css', false) ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo JRoute::_('components/com_wicitswinerycoop/css/jquery-ui-custom.min.css', false) ?>" />

	<form action="<?php echo JRoute::_('index.php', false) ?>" method="post" id="invoice_form">
		<div class="form_header">
			<h2>Badger State Winery Cooperative</h2>
			<div>
				<label for="date">Date:</label>
				<input type="text" id="date" name="date" value="<?php echo $this->invoice->date; ?>"/>			
				<label for="invoicenum">Invoice #:</label>
				<input type="text" id="invoicenum" name="invoicenum" class="autofill" disabled="disabled" value="<?php echo $this->invoice->winery.$this->invoice->order_num; ?>" />
			</div>
		</div>
		<div class="invoice_info">
			<h3>Invoice</h3>
			<div class="billing">
				<div>
					<label for="billto">Bill To:</label>
					<input type="text" id="billto" name="billto" value="<?php echo $this->invoice->bill_to; ?>" />
					<ul id="billto_list">
	<?php foreach($this->businesses as $biz) : ?>
						<li><span><?php echo $biz->name ?></span>
							<ul>
								<li class="biz_address"><?php echo $biz->address ?></li>
								<li class="biz_csz"><?php echo $biz->address2 ?></li>
								<li class="biz_resell"><?php echo $biz->reseller_num ?></li>
							</ul>
						</li>
	<?php endforeach; ?>
					</ul>
				</div>
				<div>
					<label for="address">Address:</label>
					<input type="text" id="address" name="address" value="<?php echo $this->invoice->address; ?>" />
				</div>
				<div>
					<label for="csz">City, State, Zip:</label>
					<input type="text" id="csz" name="csz" value="<?php echo $this->invoice->address2; ?>" />
				</div>
				<div>
					<label for="resellernum">Reseller's Number:</label>
					<input type="text" id="resellernum" name="resellernum" value="<?php echo $this->invoice->reseller_num; ?>" />
				</div>
			</div>
			<div class="winery">
				<div>
					<label for="winery">Winery Name:</label>
	<?php	if($this->user->isAdmin):?>		
					<select name="winery" id="winery">
						<option value="0">-Select-</option>
	<?php
		foreach($this->wineries as $winery) :
			$selected = $winery->abbr == $this->invoice->winery ? '" selected="selected' : '';
	?>
						<option value="<?php echo $winery->abbr.$selected?>"><?php echo $winery->name?></option>
	<?php endforeach;?>
					</select>
	<?php  else: ?>
					<input type="text" disabled="disabled" value="<?php echo $this->user->winery_name ?>" />
					<input type="hidden" value="<?php echo $this->user->abbr ?>" name="winery" id="winery" />
	<?php endif; ?>
				</div>
				<div>
					<label for="phone">Phone:</label>
					<input type="text" id="phone" name="phone" value="<?php echo $this->invoice->phone; ?>" />
				</div>
				<div>
					<label for="ordernum">Order Number:</label>
					<input type="text" id="ordernum" name="ordernum" value="<?php echo $this->invoice->order_num; ?>" />
				</div>
			</div>
		</div>
		<div class="controls">
			<input type="button" id="del_row" value="Remove Item(s)" />
			<input type="button" id="add_row" value="Add Item" />
		</div>
		<table class="order_table">
			<thead>
				<tr>
					<th class="th_i"><input type="checkbox" id="select_all" /></th>
					<th class="th_q">Qty</th>
					<th class="th_v">Volume</th>
					<th class="th_d">Description</th>
					<th class="th_c">Beverage Class</th>
					<th class="th_p">Unit Price</th>
					<th class="th_t">Total</th>
				</tr>
			</thead>
			<tbody>
	<?php 
		$i = 0;
		foreach ($this->invoice->items as $item) :
	?>
				<tr>
					<td><input type="checkbox" name="row<?php echo $i ?>" /></td>
					<td><input type="text" name="quantity[<?php echo $i ?>]" class="int" value="<?php echo $item->qty ?>" /></td>
					<td>
						<select name="volume[<?php echo $i ?>]">
							<option value="0">-Select-</option>
							<option value="0.750"<?php if($item->volume == .75) echo ' selected="selected"';?>>750mL</option>
							<option value="0.375"<?php if($item->volume == .375) echo ' selected="selected"';?>>375mL</option>
							<option value="0.187"<?php if($item->volume == .187) echo ' selected="selected"';?>>187mL</option>
							<option value="0.355"<?php if($item->volume == .355) echo ' selected="selected"';?>>12oz</option>
							<option value="0.473"<?php if($item->volume == .473) echo ' selected="selected"';?>>16oz</option>
						</select>
					</td>
					<td><input type="text" name="description[<?php echo $i ?>]" value="<?php echo $item->description ?>" /></td>
					<td>
						<select name="bevclass[<?php echo $i ?>]">
							<option value="0">-Select-</option>
							<option value="1"<?php if($item->beverage_class == 1) echo ' selected="selected"';?>>Apple Cider 7% or less</option>
							<option value="2"<?php if($item->beverage_class == 2) echo ' selected="selected"';?>>Wine 14% or less</option>
							<option value="3"<?php if($item->beverage_class == 3) echo ' selected="selected"';?>>Wine over 14%</option>
						</select>
					</td>
					<td><input type="text" name="unitprice[<?php echo $i ?>]" class="num" value="<?php echo number_format($item->unit_price, 2) ?>" /></td>
					<td><input type="text" name="itemtotal[<?php echo $i ?>]" class="autofill" disabled="disabled" value="<?php echo number_format($item->unit_price * $item->qty, 2) ?>" /></td>
				</tr>
	<?php 
			$i++;
		endforeach;
	?>
			</tbody>
			<tfoot>
				<tr>
					<td class="empty" colspan="4"> </td>
					<th colspan="2">Total Due</th>
					<td><input type="text" name="subtotal" class="autofill" disabled="disabled" value="0.00"/></td>
				</tr>
			</tfoot>
		</table>
		<div class="controls">
			<input type="submit" value="Save Invoice" id="invoice_submit" name="do"/>
			<a href="<?php echo JRoute::_('index.php?option=com_wicitswinerycoop&view=invoice&tmpl=component&id='.$this->invoice->id, false) ?>" target="_blank" id="invoice_print">
				Print Invoice<br/><span>Unsaved Changes will be lost</span>
			</a>
		</div>
		<input type="hidden" name="option" value="com_wicitswinerycoop" />
		<input type="hidden" name="task" value="saveinvoice" />
	</form>
	<div class="modal">
		<h3>Error: Invalid Fields</h3>
		<p>
			There were some errors found in your form. The fields that need to be changed are highlighted. 
			Hover your cursor over the field for more details. Please resolve all of the issues before
			trying to submit this form again.
		</p>
		<a href="#" class="simplemodal-close close-button" title="Close Dialog Box">X</a>
	</div>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/jquery.wtooltip.min.js', false) ?>"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/jquery.simplemodal.min.js', false) ?>"></script>
	<script type="text/javascript" src="<?php echo JRoute::_('components/com_wicitswinerycoop/js/invoice.js', false) ?>"></script>
<?php
else :
?>
	<div style="background-color:#FEF1EC; border:1px solid red; color:red; padding:15px;">
		<h2>Error Retrieving Invoice</h2>
		<p>
			Sorry, we could not find an invoice matching the specified ID. Either the invoice was deleted or you typed the URL in wrong.
		</p>
	</div>
<?php
endif;
?>