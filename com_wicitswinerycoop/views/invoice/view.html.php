<?php
/**
 * @package		Wi-CITS Business Partners
 * @version		1.0 stable
 * @copyright	Copyright(C) 2010 Wi-CITS. All rights reserved.
 * @author		Joseph Zimmerman (wicits@uww.edu)
 * @license		Commercial (visit http://wicits.uww.edu for additional information)
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Partners component
 *
 * Prepares all data to be used in the html 
 */
class WineryViewInvoice extends JView
{
	function display( $tpl = null)
	{		
		$session  = JFactory::getSession();
		$user     = $session->get('winery_user', null);	
		
		$id       = JRequest::getVar('id', null);
		$isNew    = !$id;
		
		if (!$isNew) {
			$invoice =& $this->get('data', 'invoice');
			
			if ($invoice->date) {
				jimport('joomla.utilities.date');
				$invoice->date = new JDate($invoice->date);
				$invoice->date = $invoice->date->toFormat('%Y-%m-%d');
			}
			
			$this->assign('invoice', $invoice);
		}
		else {
			$data = $session->get('postData', null);
			$session->set('postData', null);
			
			$this->assignRef('data', $data);
		}
		
		$wineries   =& $this->get('data', 'wineries');
		$model      =& $this->getModel('businesses');
		$businesses =& $model->getData($user);
		
		$this->assignRef('user',       $user);
		$this->assignRef('isNew',      $isNew);
		$this->assignRef('invoice',    $invoice);
		$this->assignRef('wineries',   $wineries);
		$this->assignRef('businesses', $businesses);
		
		if (JRequest::getVar('tmpl', '') == 'component') {
			$tpl = 'print';			
			$alcohol = array(0,0,0);
			
			foreach ($wineries as $winery) {
				if ($winery->abbr == $invoice->winery) {
					$invoice->wineryname = $winery->name;
					break;
				}
			}
			
			foreach ($invoice->items as &$item) {
				switch ($item->volume) {
					case .75:
						$item->volumeString = '750mL'; break;
					case .375:
						$item->volumeString = '375mL'; break;
					case .187:
						$item->volumeString = '187mL'; break;
					case .355:
						$item->volumeString = '12oz'; break;
					case .473:
						$item->volumeString = '16oz'; break;
				}
				
				switch ($item->beverage_class) {
					case 1:
						$item->bevString = 'Apple Cider 7% or less';
						$alcohol[0] += ($item->qty * $item->volume);
						break;
					case 2:
						$item->bevString = 'Wine 14% or less';
						$alcohol[1] += ($item->qty * $item->volume);
						break;
					case 3:
						$item->bevString = 'Wine over 14%';
						$alcohol[2] += ($item->qty * $item->volume);
						break;
				}
			}
			
			$this->assignRef('alcohol', $alcohol);
		}
		
		parent::display($tpl);
	}
}
?>
